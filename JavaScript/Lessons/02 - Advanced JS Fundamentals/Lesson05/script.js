function Person(firstName, lastName){

	if(this === window){
		return new Peron(firstName, lastName);
	}

	this.firstName = firstName;
	this.lastName = lastName;

	/*
	this.getFullName = function(){
				return this.firstName + " " + this.lastName;
	};

	this.greet = function(person){
				if(person instanceof Person){
					return "Hello,  " + person.getFullName() + "  !";
				} else {
					return "Hello, there!";
				}
	};*/

}

Person.prototype.getFullName = function(){
		return this.firstName + " " + this.lastName;
	};

Person.prototype.greet = function(person){
		if(person instanceof Person){
			return "Hello,  " + person.getFullName() + "  !";
		} else {
			return "Hello, there!";
		}
	};

/*function createPerson(firstName, lastName){
	return {
		firstName: firstName,
		lastName: lastName,
		getFullName: function(){
				return this.firstName + " " + this.lastName;
			}, 
		greet: function(person){
				if(typeof person.getFullName !== "undefined"){
					return "Hello,  " + person.getFullName() + "  !";
				} else {
					return "Hello, there!";
				}
			}
	};
}*/

/*var person = createPerson("John", "Doe"),
	person2 = createPerson("Jane", "Doe");
*/

var person = new Person("John", "Doe"),
	person2	= new Person("Jane", "Doe");

//alert(person.greet(person2));
alert(person.greet({}));