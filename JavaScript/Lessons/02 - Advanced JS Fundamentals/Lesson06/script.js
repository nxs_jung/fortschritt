var calculate = function(){
	var fn = Array.prototype.pop.apply(arguments);
	return fn.apply(null, arguments);
};

var sum = function(){
		var total = 0;
		for (var i = 0, l = arguments.length; i < l; i = i + 1){
			total = total + arguments[i];
		}
		return total;
	};

var	diff = function(){
		var total = arguments[0];
		for (var i = 1, l = arguments.length; i < l; i = i + 1){
			total = total - arguments[i];
		}
		return total;
	};

var sumResult = calculate(1,2,3,4,5, sum),
	diffResult = calculate(1,2, diff);

alert(sumResult);
alert(diffResult);