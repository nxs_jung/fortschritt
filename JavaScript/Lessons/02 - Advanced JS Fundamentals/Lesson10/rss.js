var rss = {
	channel : {
		title : "RSS Sample",
		description : "A sample RSS Feed",
		link : "http://www.nexus-netsoft.com",
		lastBuildDate : "Wed, 24 Sep 2014 12:28:00 -0100",
		pubDate : "Wed, 24 Sep 2014 12:28:00 -0100",
		items : [
			{
				title : "An Item's Title",
				description : "An item's description",
				link : "http://www.nexus-netsoft.com/sample",
				pubDate : "Wed, 24 Sep 2014 12:28:00 -0100"
			}
		]
	}
};