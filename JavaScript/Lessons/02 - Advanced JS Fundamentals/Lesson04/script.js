/*var calculator = {
	calculate: function(x, y, fn){
			return fn(x,y);
		}
	};

var sum = function(x, y){
		return x+y;
	},
	diff = function(x, y){
		return x-y;
	};

var sumResult = calculator.calculate(1,2, sum),
	diffResult = calculator.calculate(1,2, diff);

alert(sumResult);
alert(diffResult);*/

var fruit = ["apples", "oranges", "bananas", "grapes"/*,"oranges"*/];

/*var index = fruit.lastIndexOf("oranges");

alert(index);*/

function isString(value, index, array){
	return typeof value === "string";
};

function startsWithG(value, index, array){
	return value[0] === "g";
};

function isLengthOfOne(value, index, array){
	return value.length === 1;
}

function startsWithAB(value, index, array){
	return value[0] === "a" || value[0] === "b";
};

function doSomething(value, index, array){ //forEach
	alert(value);
}

function doSomethingElse(value, index, array){ //map
	return "I like " + value;
}

//fruit.forEach(doSomething);

var result = fruit.map(doSomethingElse);

alert(result);