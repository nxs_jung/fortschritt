var container = $('div.slider_viewer').css('overflow', 'hidden').children('ul'),
    slider = new Slider(container, $('.slider'));

$( document ).ready(slider.init());

slider.nav.find('.nav').on('click', function() {
  slider.stopSlider();
	slider.run($(this).data('dir'), 'slide');
});

$(document).keydown(function(e){
 
    //e.which is set by jQuery for those browsers that do not normally support e.keyCode.
    var keyCode = e.keyCode || e.which;

    if (keyCode == 37){
      slider.stopSlider();
      slider.run('prev', 'slide');
    	return false;
    }else if (keyCode == 39){
      slider.stopSlider();
      slider.run('next', 'slide');
    	return false;
    }
 
});

slider.nav.find('.bulletIndi').on('click', function(){
  slider.stopSlider();
  slider.run($(this).data('index'));
});