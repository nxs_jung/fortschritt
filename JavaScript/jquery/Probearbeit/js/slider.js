function Slider (container, nav) {
	this.container = container;
	this.nav = nav;

	this.lis = this.container.find('li');
	this.liWidth = $(this.lis[0]).width();
	this.lisLen = this.lis.length;

	this.current = 0;
	this.interval = 0;
}

Slider.prototype.init = function() {
       var self = this;

       self.createIndicator();
       self.setIncicator();
       this.interval = setInterval(function(){
               self.autoplay()        
       }, 1500);
};

Slider.prototype.autoplay = function(){
	this.run('next', 'slide');
}

Slider.prototype.transition = function() {
	this.container.animate({
		'margin-left' : -(this.current * this.liWidth)
	});
};

Slider.prototype.slideCurrent = function(dir) {
	var pos = this.current;
	pos += (~~(dir === 'next') || -1);

	this.current = (pos < 0) ? this.lisLen - 1: pos % this.lisLen;

	return pos;
};

Slider.prototype.setCurrent = function(index) {
	return this.current = index;
};

Slider.prototype.stopSlider = function() {
	clearInterval(this.interval);
};

Slider.prototype.createIndicator = function() {
	var preNavIndi = $('<div></div>', {
					'class': 'preNavIndi'
				});

	var navIndi = $('<div></div>', {
					'class': 'navIndi'
				});

	var bulletIndi = $('<p></p>', {
					'class': 'bulletIndi'
				});

	var navWidth = 0;

	if(this.lisLen > 1){
		preNavIndi.appendTo(this.nav.find('.slider_viewer'));
		navIndi.appendTo(this.nav.find('.slider_viewer'));
		for (var i = 0, l = this.lisLen ; i < l ; i++) {
			bulletIndi.clone().attr('data-index', i).appendTo(navIndi);
		};
	};

	navWidth = navIndi.width();

	preNavIndi.css({"right": ""+navWidth+"px"});

};

Slider.prototype.clearIncicator = function(){
	this.nav.find('*[data-index="'+this.current+'"]').removeClass('active');
};

Slider.prototype.setIncicator = function(){
	this.nav.find('*[data-index="'+this.current+'"]').addClass('active');
};

Slider.prototype.run = function(dir, fn){
	this.clearIncicator();
	
	if(fn === 'slide'){
		this.slideCurrent(dir);
	}else{
		this.setCurrent(dir);
	}
	
	this.transition();
	this.setIncicator();
}