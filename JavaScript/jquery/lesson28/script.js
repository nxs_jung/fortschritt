//Utility
if(typeof Object.create !== 'function'){
	Object.create = function(obj){
		function F(){};
		F.prototype = obj;
		return new F();
	};
}

(function($, window, document, undefined){
	var Twitter = {
		init: function( options, elem ){
			var self = this;

			self.elem = elem;
			self.$elem = $(elem);

			self.url = 'http://jquery.php54.moj.nexus-developer.com/lesson28/twitter-api/auth.php';
			
			self.search = (typeof options === 'string')
				? options
				: options.search; 

			self.options = $.extend({}, $.fn.queryTwitter.options, options);

			self.cycle();
		},

		cycle: function(){
			var self= this;
			
			self.fetch().done(function(results){
				results = self.limit(results.statuses, self.options.limit);

				self.buildFrag( results );

				self.display();

				if (typeof self.options.onComplete === 'function') {
					self.options.onComplete.apply(self.elem, arguments) /*** STOPPED HERE ***/
				};
			});
		},

		fetch: function(){
			return $.ajax({
				url: this.url,
				dataType: 'json',
				data: { q: this.search },
			});
		},

		buildFrag: function(results){
			var self = this;

			self.tweets = $.map(results, function(obj, i){
				return $(self.options.wrapEachWith).append(obj.text)[0];
			});
		},

		display: function(){
			this.$elem.html(this.tweets);
		},

		limit: function(obj, count){
			return obj.slice(0, count);
		}

	};

	$.fn.queryTwitter = function( options ){
		return this.each(function(){
			var twitter = Object.create(Twitter);
			twitter.init(options, this);
		});
	};

	$.fn.queryTwitter.options = {
		search: 'Warcraft', 
		wrapEachWith: '<li></li>',
		limit: 10,
		onComplete: null
	}
})(jQuery, window, document);

