//Utility
if(typeof Object.create !== 'function'){
	Object.create = function(obj){
		function F(){};
		F.prototype = obj;
		return new F();
	};
}

(function($, window, document, undefined){
	var Slider = {
		init: function(container, nav, options){
			var self = this;

			self.container = container;
			self.nav = nav;

			self.cyclce();

			self.lis = self.container.find('li');
			self.liWidth = ($(self.lis[0]).width())+10;
			self.lisLen = self.lis.length;

			self.container.css('width', (self.liWidth*self.lisLen)+'px');

			self.current = 0;
			self.interval = 0;


			self.nav.find('.nav').on('click', function() {
				self.stopSlider();
				self.run($(this).data('dir'), 'slide');
			});

		},

		cyclce: function(){
			var self = this;

		    self.results = self.fetch();

		    self.buildFrag(self.results);


		    self.display();

		    /*
		    */
			self.interval = setInterval(function(){
		    	self.autoplay()        
		    }, 500);

		},

		fetch: function(){
			var array= [];
			for ( var i = 0; i < 151; i++){
				if((i+1)<10){
					array[i] = {
						url: "http://jquery.php54.moj.nexus-developer.com/slider_bilder_plugin/img/pokemon/00"+(i+1)+"MS.png"
						}
					}else 
						if((i+1)< 100){
							array[i] = {
								url: "http://jquery.php54.moj.nexus-developer.com/slider_bilder_plugin/img/pokemon/0"+(i+1)+"MS.png"
							}
						}else {
							array[i] = {
								url: "http://jquery.php54.moj.nexus-developer.com/slider_bilder_plugin/img/pokemon/"+(i+1)+"MS.png"
							}
						}
			}
			return array;
		},

		autoplay: function(){
			this.run('next', 'slide');
		},

		transition: function(){
			this.container.animate({
				'margin-left' : -(this.current * this.liWidth)
			}, 100);
		},

		slideCurrent: function(dir){
			var pos = this.current;
			pos += (~~(dir === 'next') || -1);

			this.current = (pos < 0) ? this.lisLen - 1: pos % this.lisLen;

			return pos;
		},


		stopSlider: function() {
			clearInterval(this.interval);
		},


		run: function(dir, fn){
			
			if(fn === 'slide'){
				this.slideCurrent(dir);
			}
			
			console.log(this);

			//this.infinity();

			this.transition();
		},

		buildFrag: function(results){
			var self = this;

			self.pictures = $.map(results, function(obj, i){
				return $('<li class="pokemon_img"></li>').append("<img src='"+obj.url+"'></img>")[0];
			});
		},

		display: function(){
			var self = this;

			self.container.html(self.pictures);

		},

		infinity: function(){
			console.log(Slider);

			console.log('infinity called!');

			console.log('counter', Slider.counter);

			if(Slider.counter >= 10){
				var width = Slider.container.children('li').first().width();

				console.log('counter', Slider.counter);

				Slider.container.children('li').first().appendTo(Slider.container);
				Slider.container.css({
					'margin-left': '+='+width
				});
			}
		},

		keypressNext: function(){
			Slider.stopSlider();
			Slider.run('next', 'slide');
		},

		keypressPrev: function(){
			Slider.stopSlider();
			Slider.run('prev', 'slide');
		},

		clickedSlide: function(dir){
			Slider.stopSlider();
			Slider.run(dir, 'slide');
		}	
	}

	$.fn.slideSlider = function(options){
		var container = this.find('.slider_viewer').css('overflow', 'hidden').children('ul');
		var nav = $(this);
		return this.each(function(){
			var slider = Object.create(Slider);
			slider.init(container, nav, options);

			$.data(this, 'slideSlider', slider);
		});
	};

	$.fn.slideSlider.options = {
		
	}
})(jQuery, window, document);