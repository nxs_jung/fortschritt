(function(){

	var TicTacToe = {

		/**
		 * User
		 * @type {Boolean}
		 */
		userO : true,
		userX : false,

		/**
		 * Siegbedingungen
		 * @type {Array}
		 */
		wins : [],

		/**
		 * Anzahl der Züge
		 * @type {Number}
		 */
		moves : 0,

		/**
		 * Kürzel des gewinnenden Spielers
		 * @type {String}
		 */
		winningPlayer : "",

		/**
		 * Punktzahl für die Ermittlung der Siegbedingung
		 * @type {Number}
		 */
		scoreO : 0,
		scoreX : 0,

		/**
		 * Intervalname der Animation
		 * @type {Number}
		 */
		interval : 0,

		/**
		 * Variable des Tablebodies
		 * @type {Number}
		 */
		tbody : 0,

		/**
		 * Variable des Captionelements
		 * @type {Number}
		 */
		userTurn : 0,

		/**
		 * Variable des Reset/New-Game buttons
		 * @type {Number}
		 */
		resetBtn : 0,

		/**
		 * Gamestart Variable
		 * @type {Boolean}
		 */
		gameStarted : true,


		
		/**
		 * Initialisiert das Spiel
		 */
		init: function(){
			var self;

			this.tbody = $('tbody'),
			this.userTurn = $('caption'),
			this.resetBtn = $('button');

			this.wins = [7, 56, 448, 73, 146, 292, 273, 84];

			self = this;
			if(this.userO === true){
					this.userTurn.text("It's your turn, Player O !");
				}else if(this.userX === true){
					this.userTurn.text("It's your turn, Player X !");
			};

			this.tbody.on('click', 'td', function(){
				if(self.gameStarted){
					self.clickTile(this);
				}
			});


			this.resetBtn.on('click', function(){
				self.resetGame(); 
			});
		},


		/**
		 * Vergleicht die Punktzahl mit der Siegbedingung
		 * @param  {Number} score
		 * @return {boolean}
		 */
		checkWin: function(score){
			for (var i = 0; i < this.wins.length; i++) {
	            if ((this.wins[i] & score) === this.wins[i]) {
	                return true;
	            }
	        }
	      	return false;
		},

		/**
		 * Aktion bei drücken des Feldes
		 * @param  {Object} element
		 */
		clickTile : function(element){
			var $this = $(element);

			this.moves += 1;

			if($this.text() === ""){
				if(this.userO === true){
					this.scoreO = this.setTile($this, "O", this.scoreO);
				}else{
					this.scoreX = this.setTile($this, "X", this.scoreX);
				} 
			
				if (this.checkWin(this.scoreO)) {
		        	this.win("O");
		        } else if(this.checkWin(this.scoreX)){
		        	this.win("X");
		        } else if (this.moves === 9) {
		            this.userTurn.text("Cat's game!");
		        } else {
					this.userO = !(this.userO);
					this.userX = !(this.userX);

					if(this.userO === true){
						this.userTurn.text("It's your turn, Player O !");
					}else if(this.userX === true){
						this.userTurn.text("It's your turn, Player X !");
					}
		        }
			}
		},

		/**
		 * Makiert das entsprechende Feld für den User und schreibt die Punkte gut
		 * @param {Object} $this
		 * @param {String} shortCut
		 * @param {Number} score
		 */
		setTile: function($this, shortCut, score){
			$this
				.text(''+shortCut)
				.addClass('clicked');
			score += $this.data('score');
			return score;
		},

		/**
		 * Gibt den Gewinner bekannt, startet die Animation und beendet das Spiel
		 * @param  {String} shortCut
		 */
		win : function(shortCut){
			this.userTurn.text("Player " + shortCut + " win's !");
            this.winningPlayer = "" + shortCut + "";
            this.interval = this.snow({newOn: 100});
            this.gameStarted = false;
		},

		/**
		 * Reseted/Startet das Spiel neu
		 */
		resetGame : function(){
			this.userO = !(this.userO),
			this.userX = !(this.userX),
			this.moves = 0,
			this.winningPlayer = "",
			this.scoreO = 0,
			this.scoreX = 0,
			this.gameStarted = true;

			clearInterval(this.interval);

			if(this.userO === true){
				this.userTurn.text("It's your turn Player O !");
			}else{
				this.userTurn.text("It's your turn Player X !");
			}

			this.tbody
					.find('td')
						.removeClass('clicked')
						.text('');
		},

		/**
		 * Gewinnanimation mit dem entsprechenden User-Kürzel
		 * @param  {Object} options
		 */
		snow : function(options){
			var $flake 			= $('<div class="flake" />').css({'position': 'absolute', 'top': '-50px'}).html(''+this.winningPlayer),
				documentHeight 	= $(document).height(),
				documentWidth	= $(document).width(),
				defaults		= {
									minSize		: 10,
									maxSize		: 20,
									newOn		: 500,
									flakeColor	: "#"+(Math.floor(Math.random() * 9))+""+(Math.floor(Math.random() * 9))+""+(Math.floor(Math.random() * 9))+""+(Math.floor(Math.random() * 9))+""+(Math.floor(Math.random() * 9))+""+(Math.floor(Math.random() * 9))+""
								},
				options			= $.extend({}, defaults, options);
				
			
			var interval		= setInterval( function(){
				var startPositionLeft 	= Math.random() * documentWidth - 100,
				 	startOpacity		= 1,
					sizeFlake			= options.minSize + Math.random() * options.maxSize,
					endPositionTop		= documentHeight - 40,
					endPositionLeft		= startPositionLeft - 100 + Math.random() * 200,
					durationFall		= documentHeight * 10 + Math.random() * 50;
					flakeColor			= "#"+(Math.floor(Math.random() * 9))+""+(Math.floor(Math.random() * 9))+""+(Math.floor(Math.random() * 9))+""
				$flake
					.clone()
					.appendTo('body')
					.css(
						{
							left: startPositionLeft,
							opacity: startOpacity,
							'font-size': sizeFlake,
							color: flakeColor
						}
					)
					.animate(
						{
							top: endPositionTop,
							left: endPositionLeft,
							opacity: 0.2
						},
						durationFall,
						'linear',
						function() {
							$(this).remove()
						}
					);
				}, options.newOn);
			return interval;
		}

	}

	/**
	 * Ruft das Spiel auf
	 */
	TicTacToe.init();

}());