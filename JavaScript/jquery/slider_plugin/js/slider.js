//Utility
if(typeof Object.create !== 'function'){
	Object.create = function(obj){
		function F(){};
		F.prototype = obj;
		return new F();
	};
}

(function($, window, document, undefined){
	var Slider = {
		init: function(container, nav, options){
			var self = this;

			self.container = container;
			self.nav = nav;

			self.lis = self.container.find('li');
			self.liWidth = $(self.lis[0]).width();
			self.lisLen = self.lis.length;

			self.current = 0;
			self.interval = 0;

			self.cylce();

			self.nav.find('.nav').on('click', function() {
				self.stopSlider();
				self.run($(this).data('dir'), 'slide');
			});

			self.nav.find('.bulletIndi').on('click', function(){
				self.stopSlider();
				self.run($(this).data('index'));
			});
		},

		cylce: function(){
			var self = this;

		    self.createIndicator();
		    self.setIncicator();
		    this.interval = setInterval(function(){
		    	self.autoplay()        
		    }, 1500);
		},

		autoplay: function(){
			this.run('next', 'slide');
		},

		transition: function(){
			this.container.animate({
				'margin-left' : -(this.current * this.liWidth)
			});
		},

		slideCurrent: function(dir){
			var pos = this.current;
			pos += (~~(dir === 'next') || -1);

			this.current = (pos < 0) ? this.lisLen - 1: pos % this.lisLen;

			return pos;
		},

		setCurrent: function(index) {
			return this.current = index;
		},

		stopSlider: function() {
			clearInterval(this.interval);
		},

		createIndicator: function() {
			var preNavIndi = $('<div></div>', {
							'class': 'preNavIndi'
						});

			var navIndi = $('<div></div>', {
							'class': 'navIndi'
						});

			var bulletIndi = $('<p></p>', {
							'class': 'bulletIndi'
						});

			var navWidth = 0;

			if(this.lisLen > 1){
				preNavIndi.appendTo(this.nav.find('.slider_viewer'));
				navIndi.appendTo(this.nav.find('.slider_viewer'));
				for (var i = 0, l = this.lisLen ; i < l ; i++) {
					bulletIndi.clone().attr('data-index', i).appendTo(navIndi);
				};
			};

			navWidth = navIndi.width();

			preNavIndi.css({"right": ""+navWidth+"px"});

		},

		clearIncicator: function(){
			this.nav.find('*[data-index="'+this.current+'"]').removeClass('active');
		},

		setIncicator: function(){
			this.nav.find('*[data-index="'+this.current+'"]').addClass('active');
		},

		run: function(dir, fn){
			this.clearIncicator();
			
			if(fn === 'slide'){
				this.slideCurrent(dir);
			}else{
				this.setCurrent(dir);
			}
			
			this.transition();
			this.setIncicator();
		},

		keypressNext: function(){
			Slider.stopSlider();
			Slider.run('next', 'slide');
		},

		keypressPrev: function(){
			Slider.stopSlider();
			Slider.run('prev', 'slide');
		},

		clickedSlide: function(dir){
			Slider.stopSlider();
			Slider.run(dir, 'slide');
		},

		clickedBullet: function(index){
			Slider.stopSlider();
			Slider.run(index);
		}		
	}

	$.fn.slideSlider = function(options){
		var container = this.find('.slider_viewer').css('overflow', 'hidden').children('ul');
		var nav = $(this);
		return this.each(function(){
			var slider = Object.create(Slider);
			slider.init(container, nav, options);

			$.data(this, 'slideSlider', slider);
		});
	};

	$.fn.slideSlider.options = {
		
	}
})(jQuery, window, document);