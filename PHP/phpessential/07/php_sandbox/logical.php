<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Logical Expressions</title>
</head>
<body>
    <?php
        $a = 4;
        $b = 2;

        if($a > $b){
            echo "a is larger than b";
        }elseif ($a == $b) {
            echo "a equals b";
        } else {
            echo "a is not larger than b";
        }

        //-----------------------------------------------------------//

        echo "<br />";

        //-----------------------------------------------------------//

        $c = 1;
        $d = 20;

        if(($a > $b) && ($c > $d)) {
            echo "a is larger than b AND";
            echo "c is larger than d";
        }

        if(($a > $b) || ($c > $d)) {
            echo "a is larger than b OR";
            echo "c is larger than d";
        } else {
            echo "neither a is larger than OR c is larger than d";
        }

        //-----------------------------------------------------------//

        echo "<br />";

        //-----------------------------------------------------------//

        unset($a);
        if(!isset($a)){
            $a = 100;
        }
        echo $a;

        //-----------------------------------------------------------//

        echo "<br />";

        //-----------------------------------------------------------//

        if (is_int($a)) {
            settype($a, "string");
        }
        echo gettype($a);
    ?>
</body>
</html>