<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Functions</title>
</head>
<body>
    <?php

        function say_hello(){                       //you can not overwrite it by declaring it again
            echo "Hello World! <br />";
        }

        say_hello();

        function say_hello2($word){                 //pass arguments to a function to make it more flexible
            echo "Hello {$word}! <br />";
        }

        say_hello2("Timme!");
        say_hello2("Everyone");

        $name = "John Doe";
        say_hello2($name);

        function say_hello3($greeting, $target, $punct){
            echo $greeting . ", " . $target . $punct . "<br />";
        }

        say_hello3("Greetings",$name,"!");

        //--------------------------------------------------------------//

        echo "<br />";

        //--------------------------------------------------------------//

        function addition($val1, $val2){
            $sum = $val1 + $val2;
            return $sum;
        }

        $new_val = addition(3,4);
        echo $new_val . "<br />";

        function add_sub($val1, $val2){
            $add = $val1 + $val2;
            $subt = $val1 - $val2;
            $results = array($add, $subt);
            return $results;
        }

        $result_array = add_sub(10,5);
        echo "Add: " . $result_array[0] . "<br />";
        echo "Subtraction: " . $result_array[1] . "<br />";
    ?>
</body>
</html>