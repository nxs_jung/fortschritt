<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Functions 2</title>
</head>
<body>
    <?php

        //using globals
        $bar = "outside";

        function foo(){
            global $bar;
            $bar = "inside";
        }

        foo();
        echo $bar . "<br />";


        //--------------------------------------------------------------//

        echo "<br />";

        //--------------------------------------------------------------//


        //using returns
        $bar = "outside";

        function foo2($var){
            return $var = "inside";
        }

        $bar = foo2($bar);
        echo $bar . "<br />";

        //--------------------------------------------------------------//

        echo "<br />";

        //--------------------------------------------------------------//

        function paint($room = "office", $color = "red"){           //$argument = "defaultvalue" - define default
            echo "The color of the {$room} is {$color} <br />";     //values for function arguments
        }

        paint();
        paint("bedroom","blue");
    ?>
</body>
</html>