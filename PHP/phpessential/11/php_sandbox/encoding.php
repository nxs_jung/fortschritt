<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Encoding</title>
</head>
<body>

    <?php
        $url_page = 'php/created/page/url.php';
        $param1 = 'this is a string';
        $param2 = '"bad"/<>characters$'
        $linktext = "<Click> & you'll see";

        $url = "http://phpessential.php54.moj.nexus-developer.com/11/php_sandbox/";
        $url .= rawurlencode($url_page);
        $url .= "?param1=" . urlencode($param1);
        $url .= "?param2=" . urlencode($param2);
    ?>

    <a href="
        <?php echo htmlspecialchars($url); ?>
    ">
        <?php echo htmlspecialchars($linktext); ?>
    </a>

</body>
</html>
