<?php
    require_once("includes/session.php");
    confirm_logged_in();
    confirm_permission();
    require_once("includes/connection.php");
    require_once("includes/functions.php");

    if (intval($_GET['u_id']) == 0) {
        redirect_to("user_control.php");
    }

    $id = mysql_prep($_GET['u_id']);


    if ($username = get_username_by_id($id)) {
        $query = "DELETE FROM users WHERE id=".$id." LIMIT 1";
        $result = mysql_query($query, $connection);

        if (mysql_affected_rows() == 1){
            // Success
            redirect_to("user_control.php?u_d=1");
        }else{
            // Deletion Failed
            echo "<p>Page deletion failed.</p>";
            echo "<p>" . mysql_error() . "</p>";
            echo "<a href=\"user_control.php\">Return to Usercontrol Center</a>";
        }
    }else{
        // subject didn't exist in database
        redirect_to("user_control.php");
    }

    mysql_close($connection);