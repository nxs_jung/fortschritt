<?php
    require_once("includes/session.php");
    confirm_logged_in();
    confirm_permission();
    require_once("includes/connection.php");
    require_once("includes/functions.php");
    include_once("includes/form_functions.php");

    if (isset($_GET['u_id'])) {
        $username = get_username_by_id($_GET['u_id']);
        $role = get_role_by_id($_GET['u_id']);
    }

    if (intval($_GET['u_id']) == 0) {
        redirect_to("user_control.php");
    }


    if (isset($_POST['submit'])) {
        $username = get_username_by_id($_GET['u_id']);

        //Wenn Benutzer und Password geändert werden dann ...
        if (($_POST['new_username'] != $username) && ($_POST['new_pw'] != "") && ($_POST['new_username'] != ""))
        {
            $errors = array();

            // Form Validation
            $required_fields = array('new_pw');
            $errors = array_merge($errors, check_required_fields($required_fields));

            $id = mysql_prep($_GET['u_id']);
            $username_old = get_username_by_id($id);

            $username_new = trim(mysql_prep($_POST['new_username']));
            $username_new_re = trim(mysql_prep($_POST['new_username_retype']));

            $new_password = trim(mysql_prep($_POST['new_pw']));
            $h_new_password = sha1($new_password);
            $new_password_re = trim(mysql_prep($_POST['new_pw_retype']));
            $h_new_password_re = sha1($new_password_re);

            $role = trim(mysql_prep($_POST['role']));

            $errors = array_merge($errors, validate_username($username_new));

            if (empty($errors) && ($new_password == $new_password_re) && ($username_new == $username_new_re)) {
                //Perform Update

                 $query = "UPDATE users SET
                            username = '".$username_new."',
                            hashed_password = '".$h_new_password."',
                            role = '".$role."'
                          WHERE id = ".$id;

                $result = mysql_query($query, $connection);
                if (mysql_affected_rows() == 1){
                    // Success
                    $message = "The user was sucessfully updated";
                }else{
                    // Failed
                    $message = "The user update failed.";
                    $message .= "<br />" . mysql_error();
                }
            }else{
                //Errors occured
                $message = "There were ".count($errors)." errors in the form.";
            }
        }else

        //Wenn nur das Password geändert wird dann ...
        if ((isset($_POST['new_pw'])) && ($_POST['new_pw'] != ""))
        {
            $errors = array();

            // Form Validation
            $required_fields = array('new_pw');
            $errors = array_merge($errors, check_required_fields($required_fields));

            $id = mysql_prep($_GET['u_id']);

            $new_password = trim(mysql_prep($_POST['new_pw']));
            $h_new_password = sha1($new_password);
            $new_password_re = trim(mysql_prep($_POST['new_pw_retype']));
            $h_new_password_re = sha1($new_password_re);

            $role = trim(mysql_prep($_POST['role']));


            if (empty($errors) && ($new_password == $new_password_re)) {
                //Perform Update

                 $query = "UPDATE users SET
                            hashed_password = '".$h_new_password."',
                            role = '".$role."'
                          WHERE id = ".$id;

                $result = mysql_query($query, $connection);
                if (mysql_affected_rows() == 1){
                    // Success
                    $message = "The user was sucessfully updated";
                }else{
                    // Failed
                    $message = "The user update failed.";
                    $message .= "<br />" . mysql_error();
                }
            }else{
                //Errors occured
                $message = "There were ".count($errors)." errors in the form.";
            }
        }else

        //Wenn nur der Benutzer geändert wird dann ...
        if (($_POST['new_username'] != $username) && ($_POST['new_username'] != ""))
        {
            $errors = array();

            // Form Validation
            $required_fields = array('password');
            $errors = array_merge($errors, check_required_fields($required_fields));

            $id = mysql_prep($_GET['u_id']);
            $username_old = get_username_by_id($id);

            $username_new = trim(mysql_prep($_POST['new_username']));
            $username_new_re = trim(mysql_prep($_POST['new_username_retype']));

            $role = trim(mysql_prep($_POST['role']));

            $errors = array_merge($errors, validate_username($username_new));

            if (empty($errors) && ($username_new == $username_new_re)) {
                //Perform Update

                 $query = "UPDATE users SET
                            username = '".$username_new."',
                            role = '".$role."'
                          WHERE id = ".$id;

                $result = mysql_query($query, $connection);
                if (mysql_affected_rows() == 1){
                    // Success
                    $message = "The user was sucessfully updated";
                }else{
                    // Failed
                    $message = "The user update failed.";
                    $message .= "<br />" . mysql_error();
                }
            }else{
                //Errors occured
                $message = "There were ".count($errors)." errors in the form.";
            }
        }else

        //Wenn nur die Rolle geändert wird dann ...
        if (isset($_POST['role']))
        {
            $errors = array();

            // Form Validation
            $required_fields = array('role');
            $errors = array_merge($errors, check_required_fields($required_fields));

            $id = mysql_prep($_GET['u_id']);
            $username_old = get_username_by_id($id);

            $role = trim(mysql_prep($_POST['role']));


            if (empty($errors)) {
                //Perform Update

                 $query = "UPDATE users SET
                            role = '".$role."'
                          WHERE id = ".$id;

                $result = mysql_query($query, $connection);
                if (mysql_affected_rows() == 1){
                    // Success
                    $message = "The user was sucessfully updated";
                }else{
                    // Failed
                    $message = "The user update failed.";
                    $message .= "<br />" . mysql_error();
                }
            }else{
                //Errors occured
                $message = "There were ".count($errors)." errors in the form.";
            }
        }


    } //end : if(isset($_POST['submit']))


    include("includes/header.php");
?>
<table id="structure">
    <tr>
        <td id="navigation">

        </td>
        <td id="page">
            <h2>Edit User : <?php echo $username; ?></h2>

            <?php
                if(!empty($message)){
                    echo "<p class=\"message\">" .$message. "</p>";
                }
            ?>
            <?php
                if (!empty($errors)) {
                    echo "<p class=\"errors\">";
                    echo "Please review the following fields: <br />";
                    foreach ($errors as $error) {
                        echo " - " . $error . "<br />";
                    }
                    echo "</p>";
                }
            ?>

            <form action="edit_user.php?u_id=<?php echo $_GET['u_id']; ?>" method="post">

                <h3>Wollen Sie den Benutzernamen ändern?</h3>
                Neuer Benutzername:
                <input type="text" name="new_username" id="new_username" value="">

                <br />

                Neuen Benutzernamen wiederholen:
                <input type="text" name="new_username_retype" id="new_username_retype" value="">

                <br><br><br>

                <h3>Wollen Sie das Passwort ändern?</h3>
                Neues Passwort:
                <input type="password" name="new_pw" id="new_pw" value="">

                <br />

                Neues Passwort wiederholen:
                <input type="password" name="new_pw_retype" id="new_pw_retype" value="">

                <?php
                if($role != "SUPERADMIN"){
                ?>

                <p>
                    Rolle:
                    <select name="role" >
                          <option value="user" <?php
                            if($role == "user"){echo "selected";}
                            ?> >User</option>
                          <option value="admin" <?php
                            if($role == "admin"){echo "selected";}
                            ?> >Admin</option>
                    </select>
                </p>

                <?php
                }
                ?>

                <br>
                <input type="submit" name="submit" value="Aktualisieren!">

            </form>

            <br />
            <a href="user_control.php">Cancel</a>
        </td>
    </tr>
</table>
<?php
    require("includes/footer.php");
?>

