<?php
    require_once("includes/session.php");
    confirm_logged_in();
    require_once("includes/connection.php");
    require_once("includes/functions.php");

    if (intval($_GET['subj']) == 0) {
        redirect_to("content.php");
    }

    include_once("includes/form_functions.php");

    if (isset($_POST['submit'])) {
        $errors = array();

        // Form Validation
        $required_fields = array('menu_name','position', 'visible'  );
        $errors = array_merge($errors, check_required_fields($required_fields));


        $fields_with_lengths = array('menu_name' => 30);
        $errors = array_merge($errors, check_max_field_lengths($fields_with_lengths));

        if (empty($errors)) {
            //Perform Update
            $id = mysql_prep($_GET['subj']);
            $menu_name = trim(mysql_prep($_POST['menu_name']));
            $position = mysql_prep($_POST['position']);
            $visible = mysql_prep($_POST['visible']);

            $query = "UPDATE subjects SET
                        menu_name = '".$menu_name."',
                        position = ".$position.",
                        visible = ".$visible."
                      WHERE id = ".$id."
                     ";

            $result = mysql_query($query, $connection);
            if (mysql_affected_rows() == 1){
                // Success
                $message = "The subject was sucessfully updated";
            }else{
                // Failed
                $message = "The subject update failed.";
                $message .= "<br />" . mysql_error();
            }
        }else{
            //Errors occured
            $message = "There were ".count($errors)." errors in the form.";
        }


    } //end : if(isset($_POST['submit']))


    find_selected_page();

    include("includes/header.php");
?>
<table id="structure">
    <tr>
        <td id="navigation">
            <?php
                echo navigation_staff($sel_subject, $sel_page);
            ?>
            <br/>
            <a href="new_subject.php">+ Add a new subject</a>
        </td>
        <td id="page">
            <h2>Edit Subject : <?php echo $sel_subject['menu_name']; ?></h2>

            <?php
                if(!empty($message)){
                    echo "<p class=\"message\">" .$message. "</p>";
                }
            ?>
            <?php
                if (!empty($errors)) {
                    echo "<p class=\"errors\">";
                    echo "Please review the following fields: <br />";
                    foreach ($errors as $error) {
                        echo " - " . $error . "<br />";
                    }
                    echo "</p>";
                }
            ?>
            <form action="edit_subject.php?subj=<?php echo
                                            urlencode($sel_subject['id']);
                                        ?>" method="post">
                <p>
                    Subject name:
                    <input type="text" name="menu_name" value="<?php
                        echo $sel_subject['menu_name'];
                    ?>" id="menu_name" />
                </p>
                <p>
                    Position:
                    <select name="position" >
                        <?php
                            $subject_set = get_all_subjects(false);
                            $subject_count = mysql_num_rows($subject_set);

                            // $subject_count+1 b/c we are adding one subject
                            for ($count=1; $count <= ($subject_count); $count++) {
                                echo "<option value=\"".$count."\"";
                                if ($sel_subject['position'] == $count) {
                                    echo " selected";
                                }
                                echo ">".$count."</option>";
                            }
                        ?>
                    </select>
                </p>
                <p>
                    Visible:
                    <input type="radio" name="visible" value="0"
                    <?php
                        if ($sel_subject['visible'] == 0) {
                            echo "checked";
                        }
                    ?>
                     /> No
                    &nbsp;
                    <input type="radio" name="visible" value="1"
                    <?php
                        if ($sel_subject['visible'] == 1) {
                            echo " checked";
                        }
                    ?>
                     /> Yes
                </p>
                <input type="submit" name="submit" value="Edit Subject" />

                &nbsp;&nbsp;&nbsp;

                <a href="delete_subject.php?subj=<?php echo urlencode($sel_subject['id']); ?>"
                onclick="return confirm('Are you sure?'); ">Delete Subject</a>
                <p>
                    <hr />
                    Connected Pages:
                    <ul name="pages" >
                        <?php
                            $page_set = get_pages_for_subject($sel_subject['id']);


                            while ($page = mysql_fetch_array($page_set)) {
                                echo "<li><a href=\"content.php?page=" . urlencode($page["id"]) . "\">"
                                    . $page['menu_name'] .
                                "</a></li>";
                            }                        ?>
                    </ul>
                </p>
                <br/>
                + <a href="new_page.php?subj=<?php echo urlencode($sel_subject['id']); ?>">Add a new page to this subject</a>
            </form>

            <br />
            <a href="content.php">Cancel</a>
        </td>
    </tr>
</table>
<?php
    require("includes/footer.php");
?>
