<?php
    require_once("includes/session.php");
    confirm_logged_in();
    require_once("includes/connection.php");
    require_once("includes/functions.php");

    if (intval($_GET['page']) == 0) {
        redirect_to("content.php");
    }

    include_once("includes/form_functions.php");

    if (isset($_POST['submit'])) {
        $errors = array();

        // Form Validation
        $required_fields = array('menu_name','subject_id', 'position', 'visible' ,'content');
        $errors = array_merge($errors, check_required_fields($required_fields));


        $fields_with_lengths = array('menu_name' => 30);
        $errors = array_merge($errors, check_max_field_lengths($fields_with_lengths));

        $id = mysql_prep($_GET['page']);
        $menu_name = trim(mysql_prep($_POST['menu_name']));
        $subject_id = mysql_prep($_POST['subject_id']);
        $position = mysql_prep($_POST['position']);
        $visible = mysql_prep($_POST['visible']);
        $content = mysql_prep($_POST['content']);

        if (empty($errors)) {
            //Perform Update

            $query = "UPDATE pages SET
                        menu_name = '".$menu_name."',
                        position = ".$position.",
                        visible = ".$visible.",
                        content = '".$content."'
                      WHERE id = ".$id."
                       AND subject_id = ".$subject_id."
                     ";

            $result = mysql_query($query, $connection);
            if (mysql_affected_rows() == 1){
                // Success
                $message = "The page was sucessfully updated";
            }else{
                // Failed
                $message = "The page update failed.";
                $message .= "<br />" . mysql_error();
            }
        }else{
            //Errors occured
            $message = "There were ".count($errors)." errors in the form.";
        }


    } //end : if(isset($_POST['submit']))


    find_selected_page();

    include("includes/header.php");
?>
<table id="structure">
    <tr>
        <td id="navigation">
            <?php
                echo navigation_staff($sel_subject, $sel_page);
            ?>
            <br/>
            <a href="new_subject.php">+ Add a new subject</a>
        </td>
        <td id="page">
            <h2>Edit Page : <?php echo $sel_page['menu_name']; ?></h2>

            <?php
                if(!empty($message)){
                    echo "<p class=\"message\">" .$message. "</p>";
                }
            ?>
            <?php
                if (!empty($errors)) {
                    echo "<p class=\"errors\">";
                    echo "Please review the following fields: <br />";
                    foreach ($errors as $error) {
                        echo " - " . $error . "<br />";
                    }
                    echo "</p>";
                }
            ?>
            <form action="edit_page.php?page=<?php echo
                                            urlencode($sel_page['id']);
                                        ?>" method="post">

                <?php include("page_form.php"); ?>
                <input type="submit" name="submit" value="Update Page" />

                &nbsp;&nbsp;&nbsp;

                <a href="delete_page.php?page=<?php echo urlencode($sel_page['id']); ?>"
                onclick="return confirm('Are you sure?'); ">Delete Page</a>

            </form>

            <br />
            <a href="content.php">Cancel</a>
        </td>
    </tr>
</table>
<?php
    require("includes/footer.php");
?>
