<?php
require_once("includes/functions.php");

function check_required_fields($required_array){
    $field_errors = array();

    foreach ($required_array as $fieldname) {
        if (!isset($_POST[$fieldname]) || (empty($_POST[$fieldname]) && $_POST[$fieldname] != 0)
            || $_POST[$fieldname] == "") {
            $field_errors[] = $fieldname;
        }
    }

    return $field_errors;
}

function check_max_field_lengths($required_length_array){
    $field_errors = array();

    foreach ($required_length_array as $fieldname => $maxlength) {
        if (strlen(trim(mysql_prep($_POST[$fieldname]))) > $maxlength) {
            $field_errors[] = $fieldname . " LENGTH!";
        }
    }

    return $field_errors;
}

function validate_username($username){
    $username_errors = array();
    $val_username = strstr($username, "@");

    if(substr_count($val_username, ".") != 1){
        $username_errors[] = "Username: The eMail-Adress ".$username." was incorrect!";
    }

    if (check_username($username) != NULL) {
        $username_errors[] = "Username: The eMail-Adress ".$username." has been used!";
    }

    return $username_errors;
}

function display_errors($error_array){
    echo "<p class=\"errors\">";
    echo "Please review the following fields: <br />";
    foreach ($error_array as $error) {
        echo " - " . $error . "<br />";
    }
    echo "</p>";
}