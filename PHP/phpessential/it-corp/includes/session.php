<?php
session_start();
require_once("functions.php");
/**
 * checks if a session exists
 *
 * @author Moritz Jung <jung@nexus-netsoft.com>
 * @return void
 */
function logged_in(){
    return isset($_SESSION['user_id']);
}


/**
 * checks if somebody is logged in or not
 *
 * @author Moritz Jung <jung@nexus-netsoft.com>
 * @return void
 */
function confirm_logged_in(){
    if(!logged_in()){
        redirect_to("login.php");
    }
}

function is_admin(){
    return (($_SESSION['role'] == 'admin')||($_SESSION['role'] == 'SUPERADMIN'));
}

function confirm_permission(){
    if(!is_admin()){
        redirect_to("staff.php");
    }
}
