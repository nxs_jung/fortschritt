<?php
    require_once("session.php");
    require_once("constants.php");
    //This is the place to store all basic functions

    /**
     * prepares the input to be inserted into an mysql-query
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  String $value    -> the part of the query which could contain bad markups
     * @return String $value    -> converted String, ready for the query
     */
    function mysql_prep($value){
        $magic_quotes_active = get_magic_quotes_gpc();
        $new_enough_php = function_exists("mysql_real_escape_string");          // i.e. PHP >= v4.3.0

        if ($new_enough_php) {                                                  // PHP v4.3.0
                                                                                // undo any magic quote effects so
                                                                                // mysql_real_escape_string can do the work
            if ($magic_quotes_active) {
                $value = stripslashes($value);
            }
            $value = mysql_real_escape_string($value);
        }else{                                                                  // before PHP v4.3.0
                                                                                // if magic quotes aren't already on then
                                                                                // add slashes manually
            if (!$magic_quotes_active) {
                $value = addslashes($value);
            }
                                                                                // if magic quotes are active, then she
                                                                                // slases already exist
        }
        return $value;
    }

    /**
     * redirects to a site
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  String $location -> the site you want to redirect to
     */
    function redirect_to($location = NULL){
        if ($location != NULL) {
            header("Location: ".$location."");
            exit;
        }
    }

    /**
     * confirms a query and checks if there is any response from the db
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  Ressource $result_set -> the Ressource which should be checked
     * @return void
     */
    function confirm_query($result_set){
        if (!$result_set) {
            die("Database query failed: " . mysql_error());
        }
    }

    /**
     * returns all subjects from the db
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return Ressource    -> contains all subjects from the db
     */
    function get_all_subjects($public = true){
        global $connection;
        $query="SELECT *
                FROM subjects ";
        if ($public) {
            $query .= "WHERE visible = 1 ";
        }
        $query .= " ORDER BY position ASC";
        $subject_set = mysql_query($query, $connection);
        confirm_query($subject_set);
        return $subject_set;
    }


    /**
     * returns all pages for a particullar subject
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  int $subject_id       -> the id of the subject
     * @return Ressource             -> contains all pages for the subject
     */
    function get_pages_for_subject($subject_id, $public = true){
        global $connection;
        $query="SELECT *
                FROM pages
                WHERE subject_id = ".$subject_id;
        if ($public) {
            $query .= " AND visible = 1 ";
        }
        $query .= " ORDER BY position ASC";
        $page_set = mysql_query($query , $connection);
        confirm_query($page_set);
        return $page_set;
    }


    /**
     * returns all information of the subject by id
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  int $subject_id -> the id of the selected subject
     * @return array           -> an array with all information of a subject
     */
    function get_subject_by_id($subject_id){
        global $connection;

        $query = "SELECT * ";
        $query .= "FROM subjects ";
        $query .= "WHERE id=" . $subject_id . " ";
        $query .= "LIMIT 1";

        $result_set = mysql_query($query, $connection);
        confirm_query($result_set);
        if($subject = mysql_fetch_array($result_set)){
            return $subject;
        }else{
            return NULL;
        }
    }


    /**
     * returns all information of the page by id
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  int $page_id    -> the id of the selected page
     * @return array           -> an array with all information of a page
     */
    function get_page_by_id($page_id){
        global $connection;

        $query = "SELECT * ";
        $query .= "FROM pages ";
        $query .= "WHERE id=" . $page_id . " ";
        $query .= "LIMIT 1";

        $result_set = mysql_query($query, $connection);
        confirm_query($result_set);
        if($page = mysql_fetch_array($result_set)){
            return $page;
        }else{
            return NULL;
        }
    }


    function get_default_page($subject_id){
        // Get all visible pages
        $page_set = get_pages_for_subject($subject_id, true);
        if($first_page = mysql_fetch_array($page_set)){
            return $first_page;
        }else{
            return NULL;
        }
    }

    /**
     * sets the selected page/subject id
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     */
    function find_selected_page(){
        global $sel_subject;
        global $sel_page;

        if(isset($_GET['subj'])){
            $sel_subject = get_subject_by_id($_GET['subj']);
            $sel_page = NULL;
        }elseif (isset($_GET['page'])) {
            $sel_subject = NULL;
            $sel_page = get_page_by_id($_GET['page']);
        }else{
            $sel_subject = NULL;
            $sel_page = NULL;
        }
    }


    /**
     * displays the whole CRUD-Navigation
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  int $sel_subject the id of the selected subject
     * @param  int $sel_page    the id of the selected page
     * @return $output
     */
    function navigation_staff($sel_subject, $sel_page, $public = false){
        $output =  "<ul class=\"subjects\">";
                $subject_set = get_all_subjects($public);

                while ($subject = mysql_fetch_array($subject_set)) {
                    $output .= "<li ";
                    if ($subject['id'] == $sel_subject['id']) {
                        $output .= " class=\"selected \"";
                    }
                    $output .= "><a href=\"edit_subject.php?subj=". urlencode($subject['id']) ."\">"
                        . $subject['menu_name'] .
                    "</a></li>";

                    $page_set = get_pages_for_subject($subject['id'], $public);

                    $output .= "<ul class=\"pages\">";
                    while ($page = mysql_fetch_array($page_set)) {
                        $output .= "<li ";
                        if ($page['id'] == $sel_page['id']) {
                            $output .= " class=\"selected \"";
                        }
                        $output .= "><a href=\"content.php?page=" . urlencode($page["id"]) . "\">"
                            . $page['menu_name'] .
                        "</a></li>";
                    }
                    $output .= "</ul>";
                }
        $output .= "</ul>";

        $output .= "<br/> <a href=\"staff.php\">Menu</a> <br/>";

        return $output;
    }


    /**
     * displays the whole Navigation in the public area
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  int $sel_subject the id of the selected subject
     * @param  int $sel_page    the id of the selected page
     * @return $output
     */
    function navigation_public($sel_subject, $sel_page, $public = true){
        $output =  "<ul class=\"subjects\">";
        global $sel_subject;
        global $sel_page;

        if(!isset($sel_subject) && isset($sel_page)){
            $sel_subject = get_subject_by_id($sel_page['subject_id']);
        }elseif (!isset($sel_page) && isset($sel_subject)) {
            $sel_page = get_default_page($sel_subject['id']);
        }

        $subject_set = get_all_subjects($public);


        while ($subject = mysql_fetch_array($subject_set)) {
            $output .= "<li ";
            if (($subject['id'] == $sel_subject['id'])) {
                $output .= " class=\"selected \"";
            }
            $output .= "><a href=\"index.php?subj=". urlencode($subject['id']) ."\">"
                . $subject['menu_name'] .
            "</a></li>";


            if ($subject['id'] == $sel_subject['id']){
                $page_set = get_pages_for_subject($subject['id'], $public);

                $output .= "<ul class=\"pages\">";
                while ($page = mysql_fetch_array($page_set)) {
                    $output .= "<li ";
                    if ($page['id'] == $sel_page['id']) {
                        $output .= " class=\"selected \"";
                    }
                    $output .= "><a href=\"index.php?page=" . urlencode($page["id"]) . "\">"
                        . $page['menu_name'] .
                    "</a></li>";
                }
                $output .= "</ul>";
            }

        }
        $output .= "</ul>";

        if (logged_in()) {
            $output .= "<br/> <a href=\"staff.php\">Menu</a> ";
            $output .= "<br/> <a href=\"logout.php\">Logout</a> ";
        }else{
            $output .= "<br/> <a href=\"login.php\">Login</a> ";
        }

        return $output;
    }

    function check_username($username){
        global $connection;

        $query = "SELECT username ";
        $query .= "FROM users ";
        $query .= "WHERE username = '" . $username . "' ";
        $query .= "LIMIT 1";

        $result_set = mysql_query($query, $connection);
        confirm_query($result_set);
        if($username = mysql_fetch_array($result_set)){
            return $username;
        }else{
            return NULL;
        }
    }

    function check_hash($username, $id, $hash){
        if((sha1($id."".$username."".HASH)) == $hash){
            return true;
        }else{
            return false;
        }
    }

    function get_role_by_id($id){
        global $connection;

        $query = "SELECT role ";
        $query .= "FROM users ";
        $query .= "WHERE id=" . $id . " ";
        $query .= "LIMIT 1";

        $result_set = mysql_query($query, $connection);
        confirm_query($result_set);
        if($user = mysql_fetch_array($result_set)){
            return $user['role'];
        }else{
            return NULL;
        }
    }

    function get_username_by_id($id){
        global $connection;

        $query = "SELECT username ";
        $query .= "FROM users ";
        $query .= "WHERE id=" . $id . " ";
        $query .= "LIMIT 1";

        $result_set = mysql_query($query, $connection);
        confirm_query($result_set);
        if($user = mysql_fetch_array($result_set)){
            return $user['username'];
        }else{
            return NULL;
        }
    }

    function send_mail_on_usercreation($username){
        $empfaenger = $username."";
        $betreff = "Herzlich Wilkommen bei DO-it Corp";
        $from = "From: DO-iT Corp <no-reply@doit-corp.de>";
        $text = "Herzlich Wilkommen!! \n";
        $text .= "Hallo ".$username." \n".
                 "Wir wünschen Ihnen Viel Spaß auf unserer Seite!\n".
                 "http://it-corp.php54.moj.nexus-developer.com/";

        mail($empfaenger, $betreff, $text, $from);
    }

    function get_all_users(){
        global $connection;
        $query="SELECT id, username, role
                FROM users ";
        $query .= " ORDER BY id ASC";
        $subject_set = mysql_query($query, $connection);
        confirm_query($subject_set);
        return $subject_set;
    }