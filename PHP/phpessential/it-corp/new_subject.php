<?php
    require_once("includes/session.php");
    confirm_logged_in();
    require_once("includes/connection.php");
    require_once("includes/functions.php");

    find_selected_page();

    include("includes/header.php");
?>
<table id="structure">
    <tr>
        <td id="navigation">
            <?php
                echo navigation_staff($sel_subject, $sel_page);
            ?>
        </td>
        <td id="page">
            <h2>Add Subject</h2>

            <form action="create_subject.php" method="post">
                <p>
                    Subject name:
                    <input type="text" name="menu_name" value="" id="menu_name" />
                </p>
                <p>
                    Position:
                    <select name="position" >
                        <?php
                            $subject_set = get_all_subjects(false);
                            $subject_count = mysql_num_rows($subject_set);

                            // $subject_count+1 because we add one subject
                            for ($count=1; $count <= ($subject_count+1); $count++) {
                                echo "<option value=\"".$count."\">".$count."</option>";
                            }
                        ?>
                    </select>
                </p>
                <p>
                    Visible:
                    <input type="radio" name="visible" value="0" /> No
                    &nbsp;
                    <input type="radio" name="visible" value="1" /> Yes
                </p>
                <input type="submit" name="submit" value="Add Subject" />
            </form>

            <br />
            <a href="content.php">Cancel</a>
        </td>
    </tr>
</table>
<?php
    require("includes/footer.php");
?>
