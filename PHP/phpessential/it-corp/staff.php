<?php
    require_once("includes/session.php");
    confirm_logged_in();
    require_once("includes/functions.php");
    include("includes/header.php");
?>
<table id="structure">
    <tr>
        <td id="navigation">
            <a href="index.php">Back to the public Site</a>
        </td>
        <td id="page">
            <h2>Staff Menu</h2>
            <p>Welcome to the staff area, <?php echo $_SESSION['username']; ?>!</p>
            <ul>
                <li><a href="content.php">Manage Website Content</a></li>
                <?php
                if(($_SESSION['role'] == 'admin') || ($_SESSION['role'] == 'SUPERADMIN')){
                    echo "<li><a href=\"user_control.php\">Usercontrol Center</a></li>";
                }
                ?>
                <li><a href="logout.php">Logout</a></li>
            </ul>
        </td>
    </tr>
</table>
<?php
    include("includes/footer.php");
?>