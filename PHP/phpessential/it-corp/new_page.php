<?php
    require_once("includes/session.php");
    confirm_logged_in();
    require_once("includes/connection.php");
    require_once("includes/functions.php");

    find_selected_page();

    include("includes/header.php");
?>
<table id="structure">
    <tr>
        <td id="navigation">
            <?php
                echo navigation_staff($sel_subject, $sel_page);
            ?>
        </td>
        <td id="page">
            <h2>Add Page</h2>

            <form action="create_page.php" method="post">
                <?php $new_page = true; ?>
                <?php include("page_form.php"); ?>
                <input type="submit" name="submit" value="Add Subject" />
            </form>

            <br />
            <a href="content.php">Cancel</a>
        </td>
    </tr>
</table>
<?php
    require("includes/footer.php");
?>
