<?php
    require_once("includes/session.php");
    require_once("includes/connection.php");
    require_once("includes/functions.php");

    if(logged_in()){
        redirect_to("staff.php");
    }

    include_once("includes/form_functions.php");

    if(isset($_POST['submit'])){
        $errors = array();

        // Form Validation
        $required_fields = array('username', 'password');
        $errors = array_merge($errors, check_required_fields($required_fields));

        $fields_with_lengths = array('username' => 30, 'password' => 30);
        $errors = array_merge($errors, check_max_field_lengths($fields_with_lengths));


        $username = trim(mysql_prep($_POST['username']));
        $password = trim(mysql_prep($_POST['password']));
        $h_password = sha1($password);

        if (empty($errors)) {
            $query = "SELECT id, username, role
                      FROM users
                      WHERE username ='".$username."'
                      AND hashed_password ='".$h_password."'";
            $result_set = mysql_query($query, $connection);
            confirm_query($result_set);

            if (mysql_num_rows($result_set) == 1) {
                //Sucess!
                $user = mysql_fetch_array($result_set);
                $_SESSION['user_id'] = $user['id'];
                $_SESSION['username'] = $user['username'];
                $_SESSION['role'] = $user['role'];

                redirect_to("staff.php");
            }else{
                //Display ERROR
                $message = "<p> The login-process failed.</p>";
                $message .= "<p> The Username/Password combination is incorrect.<br />
                                 Please make sure your caps lock is off and try again. </p>";
            }

        }else{
            if (count($errors) == 1){
                $message = "There was 1 error in the login-form.";
            } else {
                $message = "There were " . count($errors) . " errors in the form.";
            }
        }
    }else{
        if(isset($_GET['logout']) && $_GET['logout'] == 1){
            $message = "You have successfully loged out!";
        }

        $username = "";
        $password = "";
    }

    include("includes/header.php");
?>
<table id="structure">
    <tr>
        <td id="navigation">
            <a href="index.php">Return to public site</a>
        </td>
        <td id="page">
            <h2>
                Login
            </h2>
            <?php
                if(!empty($message)){
                    echo "<p class=\"message\">" .$message. "</p>";
                }
            ?>
            <?php
                if (!empty($errors)) {
                    echo "<p class=\"errors\">";
                    echo "Please review the following fields: <br />";
                    foreach ($errors as $error) {
                        echo " - " . $error . "<br />";
                    }
                    echo "</p>";
                }
            ?>
            <form action="login.php" method="post">
                <p>
                    Username:
                    <input type="text" name="username" id="username" >
                </p>
                <p>
                    Password:
                    <input type="password" name="password" id="password" >
                </p>
                <br />
                <input type="submit" name="submit" id="submit" value="Login">
                <br />
                <a href="forgotten_pw.php">Passwort vergessen?</a>
            </form>
        </td>
    </tr>
</table>
<?php
    require("includes/footer.php");
?>
