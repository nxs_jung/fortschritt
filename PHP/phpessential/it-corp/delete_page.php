<?php
    require_once("includes/session.php");
    confirm_logged_in();
    require_once("includes/connection.php");
    require_once("includes/functions.php");

    if (intval($_GET['page']) == 0) {
        redirect_to("content.php");
    }

    $id = mysql_prep($_GET['page']);


    if ($subject = get_page_by_id($id)) {
        $query = "DELETE FROM pages WHERE id=".$id." LIMIT 1";
        $result = mysql_query($query, $connection);

        if (mysql_affected_rows() == 1){
            // Success
            redirect_to("content.php");
        }else{
            // Deletion Failed
            echo "<p>Page deletion failed.</p>";
            echo "<p>" . mysql_error() . "</p>";
            echo "<a href=\"content.php\">Return to Main page</a>";
        }
    }else{
        // subject didn't exist in database
        redirect_to("content.php");
    }

    mysql_close($connection);