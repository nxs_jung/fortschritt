<?php
    require_once("includes/session.php");
    confirm_logged_in();
    require_once("includes/connection.php");
    require_once("includes/functions.php");

    $errors = array();

    include_once("includes/form_functions.php");

    // Form Validation
    $required_fields = array('menu_name','subject_id', 'position', 'visible' ,'content');
    $errors = array_merge($errors, check_required_fields($required_fields));

    $fields_with_lengths = array('menu_name' => 30);
    $errors = array_merge($errors, check_max_field_lengths($fields_with_lengths));


    if (!empty($errors)) {
        redirect_to("new_page.php");
    }

    $menu_name = mysql_prep($_POST['menu_name']);
    $subject_id = mysql_prep($_POST['subject_id']);
    $position = mysql_prep($_POST['position']);
    $visible = mysql_prep($_POST['visible']);
    $content = mysql_prep($_POST['content']);

    $query = "INSERT INTO pages (
                menu_name, subject_id , position, visible, content
                ) VALUES (
                '".$menu_name."', ".$subject_id.", ".$position.", ".$visible.", '".$content."'
                )";

    if (mysql_query($query, $connection)) {
        //Sucess!
        redirect_to("content.php");
    }else{
        //Display ERROR
        echo "<p> Page creation failed.</p>";
        echo "<p> ".mysql_error()." </p>";
    }

    mysql_close($connection);