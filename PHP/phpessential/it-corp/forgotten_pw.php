<?php
    require_once("includes/connection.php");
    require_once("includes/functions.php");
    include("includes/header.php");
    require_once("includes/form_functions.php");
    require_once("includes/constants.php");

    $reset_pw_process = false;

    if(isset($_GET['h'])){
        $reset_pw_process = true;

        $hash = $_GET['h'];
    }

    if(isset($_POST['submit_reset'])){
        $errors = array();

        // Form Validation
        $required_fields = array('new_pw', 'new_pw_retype');
        $errors = array_merge($errors, check_required_fields($required_fields));

        $fields_with_lengths = array('new_pw' => 30, 'new_pw_retype' => 30);
        $errors = array_merge($errors, check_max_field_lengths($fields_with_lengths));

        $id = (int) substr(strstr($_POST['hash'], "|"), 1);
        $username = get_username_by_id($id);
        $hash_link = strstr($_POST['hash'], "|", true);


        if (check_hash($username, $id, $hash_link)){
            $new_password = trim(mysql_prep($_POST['new_pw']));
            $h_new_password = sha1($new_password);
            $new_password_re = trim(mysql_prep($_POST['new_pw_retype']));
            $h_new_password_re = sha1($new_password_re);


            if (empty($errors) && ($h_new_password == $h_new_password_re)) {
                $query = "UPDATE users SET
                            hashed_password = '".$h_new_password."'
                          WHERE id = ".$id;

                $result = mysql_query($query, $connection);

                if ($result) {
                    //Sucess!
                    $message = "Yout password was successfully reseted.";
                }else{
                    //Display ERROR
                    $message = "<p> Password reset failed.</p>";
                    $message .= "<p> ".mysql_error()." </p>";
                }
            }else{
                if (count($errors) == 1){
                    $message = "There was 1 error in the form.";
                } else {
                    $message = "There were " . count($errors) . " errors in the form.";
                }
            }
        }else{
            $message = "The link is not correct! \n Please try it again.";
        }
    }else if (isset($_POST['submit'])) {
        $errors = array();

        // Form Validation
        $required_fields = array('username');
        $errors = array_merge($errors, check_required_fields($required_fields));

        $fields_with_lengths = array('username' => 30);
        $errors = array_merge($errors, check_max_field_lengths($fields_with_lengths));

        if($user = check_username(trim(mysql_prep($_POST['username'])))){
            if (empty($errors)) {
                $query = "SELECT id
                          FROM users
                          WHERE username = '".$user['username']."'
                          LIMIT 1";

                $result_set = mysql_query($query, $connection);

                if ($result_set) {
                    //Sucess!
                    $result = mysql_fetch_array($result_set);

                    $hash = sha1($result['id']."".$user['username']."".HASH);
                    $hash .= "|".$result['id'];
                    $hash = mysql_prep($hash);

                    $link = "http://it-corp.php54.moj.nexus-developer.com/forgotten_pw.php?";
                    $link .= "h=".$hash;

                    $empfaenger = $user['username']."";                                     //später username
                    $betreff = "Die Mail-Reset-Funktion";
                    $from = "From: DO-iT Corp <absender@domain.de>";
                    $text = "Hier ihr Zurücksetzungslink \n ".$link;

                    mail($empfaenger, $betreff, $text, $from);

                    $message = "The mail was successfully send.";
                }else{
                    //Display ERROR
                    $message = "<p> Reset failed.</p>";
                    $message .= "<p> ".mysql_error()." </p>";
                }
            }else{
                if (count($errors) == 1){
                    $message = "There was 1 error in the form.";
                } else {
                    $message = "There were " . count($errors) . " errors in the form.";
                }
            }
        }else{
            $message = "The username does not exist!";
        }
    }
?>
<table id="structure">
    <tr>
        <td id="navigation">
            <p>
                <a href="index.php">Back to the public area</a>
            </p>
            <p>
                <a href="login.php">Back to the login area</a>
            </p>
        </td>
        <td id="page">
            <?php
                if(!empty($message)){
                    echo "<p class=\"message\">" .$message. "</p>";
                }
            ?>
            <?php
                if (!empty($errors)) {
                    echo "<p class=\"errors\">";
                    echo "Please review the following fields: <br />";
                    foreach ($errors as $error) {
                        echo " - " . $error . "<br />";
                    }
                    echo "</p>";
                }
            ?>
            <?php
                if(!$reset_pw_process){
                    ?>
                    <h2>Haben sie ihr Passwort vergessen?</h2>
                    <p>
                        Dann geben sie bitte unten ihren Benutzernamen/Emailadresse ein, um das Passwort zurückzusetzen.
                    </p>
                    <form action="forgotten_pw.php" method="post">
                        Benutzernamen/Emailadresse:
                        <input type="text" name="username" id="username">
                        <input type="submit" name="submit" value="Passwort zurücksetzen anfordern!">
                    </form>
                    <?php
                }else{
                    ?>
                    <h2>Geben sie bitte unten ihr neues Passwort ein.</h2>

                    <form action="forgotten_pw.php" method="post">
                        Neues Passwort:
                        <input type="password" name="new_pw" id="new_pw">

                        <br />

                        Neues Passwort wiederholen:
                        <input type="password" name="new_pw_retype" id="new_pw_retype">

                        <br />

                        <input type="text" name="hash" value="<?php echo $_GET['h']?>" style="display:none;">

                        <input type="submit" name="submit_reset" value="Passwort zurücksetzen!">
                    </form>
                    <?php
                }
            ?>
        </td>
    </tr>
</table>
<?php
    require("includes/footer.php");
?>
