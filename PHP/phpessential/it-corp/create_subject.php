<?php
    require_once("includes/session.php");
    confirm_logged_in();
    require_once("includes/connection.php");
    require_once("includes/functions.php");

    $errors = array();


    include_once("includes/form_functions.php");

    // Form Validation
    $required_fields = array('menu_name', 'position', 'visible');
    $errors = array_merge($errors, check_required_fields($required_fields));

    $fields_with_lengths = array('menu_name' => 30);
    $errors = array_merge($errors, check_max_field_lengths($fields_with_lengths));


    if (!empty($errors)) {
        redirect_to("new_subject.php");
    }

    $menu_name = mysql_prep($_POST['menu_name']);
    $position = mysql_prep($_POST['position']);
    $visible = mysql_prep($_POST['visible']);

    $query = "INSERT INTO subjects (
                menu_name, position, visible
                ) VALUES (
                '".$menu_name."', ".$position.", ".$visible."
                )";

    if (mysql_query($query, $connection)) {
        //Sucess!
        redirect_to("content.php");
    }else{
        //Display ERROR
        echo "<p> Subejct creation failed.</p>";
        echo "<p> ".mysql_error()." </p>";
    }

    mysql_close($connection);