<?php
    require_once("includes/session.php");
    confirm_logged_in();
    require_once("includes/connection.php");
    require_once("includes/functions.php");

    find_selected_page();

    include("includes/header.php");
?>
<table id="structure">
    <tr>
        <td id="navigation">
            <?php
                $public = false;
                echo navigation_staff($sel_subject, $sel_page);
            ?>
            <br/>
            <a href="new_subject.php">+ Add a new subject</a>
        </td>
        <td id="page">
            <h2>
                <?php
                if(!is_null($sel_subject)){
                    echo $sel_subject['menu_name'] . "</h2>";
                }elseif (!is_null($sel_page)) {
                    echo $sel_page['menu_name'] . "</h2>";
                    ?>
                    <div>
                        <?php
                            echo $sel_page['content'];
                        ?>
                        <br />
                        <a href="edit_page.php?page=<?php echo urlencode($sel_page['id']);?>">Edit this Page</a>
                    </div>
                    <?php
                }else{
                    echo "Select a subject or page to edit </h2>";
                }
                ?>
        </td>
    </tr>
</table>
<?php
    require("includes/footer.php");
?>
