<?php
    require_once("includes/connection.php");
    require_once("includes/functions.php");

    find_selected_page();

    include("includes/header.php");
?>
<table id="structure">
    <tr>
        <td id="navigation">
            <?php
                echo navigation_public($sel_subject, $sel_page);
            ?>
        </td>
        <td id="page">
            <h2>
                <?php
                if ($sel_page) {
                    echo $sel_page['menu_name'] . "</h2>";
                    ?>
                    <div class="page-content">
                        <?php
                            echo $sel_page['content'];
                        ?>
                    </div>
                    <?php
                }else{
                    echo "Welcome to DO-iT Corp </h2>";
                }
                ?>
        </td>
    </tr>
</table>
<?php
    require("includes/footer.php");
?>
