<?php
    require_once("includes/session.php");
    confirm_logged_in();
    confirm_permission();
    require_once("includes/connection.php");
    require_once("includes/functions.php");

    include_once("includes/form_functions.php");

    if(isset($_POST['submit'])){
        $errors = array();

        // Form Validation
        $required_fields = array('new_username', 'new_password');
        $errors = array_merge($errors, check_required_fields($required_fields));

        $fields_with_lengths = array('new_username' => 30, 'new_password' => 30);
        $errors = array_merge($errors, check_max_field_lengths($fields_with_lengths));


        $username = trim(mysql_prep($_POST['new_username']));
        $password = trim(mysql_prep($_POST['new_password']));
        $h_password = sha1($password);


        $errors = array_merge($errors, validate_username($username));

        if (empty($errors)) {
            if(isset($_POST['role'])){
                $role = trim(mysql_prep($_POST['role']));
                $query = "INSERT INTO users (
                          username, hashed_password, role
                          ) VALUES (
                          '".$username."', '".$h_password."', '".$role."'
                          )";
            }else{
                $query = "INSERT INTO users (
                          username, hashed_password
                          ) VALUES (
                          '".$username."', '".$h_password."'
                          )";
            }
            $result = mysql_query($query, $connection);

            if ($result) {
                //Sucess!
                $message = "The user was successfully created.";
                send_mail_on_usercreation($username);
            }else{
                //Display ERROR
                $message = "<p> User creation failed.</p>";
                $message .= "<p> ".mysql_error()." </p>";
            }
        }else{
            if (count($errors) == 1){
                $message = "There was 1 error in the form.";
            } else {
                $message = "There were " . count($errors) . " errors in the form.";
            }
        }
    }else{
        $username = "";
        $password = "";
    }

    include("includes/header.php");
?>
<table id="structure">
    <tr>
        <td id="navigation">
            <a href="staff.php">Return to Menu</a>
            <br>
            <br>
            <a href="user_control.php">Return to Usercontrol Center</a>
        </td>
        <td id="page">
            <h2>
                Create a User
            </h2>
             <?php
                if(!empty($message)){
                    echo "<p class=\"message\">" .$message. "</p>";
                }
            ?>
            <?php
                if (!empty($errors)) {
                    echo "<p class=\"errors\">";
                    echo "Please review the following fields: <br />";
                    foreach ($errors as $error) {
                        echo " - " . $error . "<br />";
                    }
                    echo "</p>";
                }
            ?>
            <form action="new_user.php" method="post">
                <p>
                    eMail-Adress (Username):
                    <input type="text" name="new_username" id="new_username" value="<?php
                        //echo htmlentities($username);
                    ?>">
                </p>
                <p>
                    Password:
                    <input type="password" name="new_password" id="new_password" value="<?php
                        //echo htmlentities($password);
                    ?>">
                </p>
                <?php
                if (is_admin()) {
                    echo "<p>";
                    echo "Role: ";

                    echo "<select name=\"role\" >";
                    echo "<option value=\"user\" >User</option>";
                    echo "<option value=\"admin\" >Admin</option>";
                    echo "</select>";

                    echo "</p>";
                }

                ?>
                <br />
                <input type="submit" name="submit" id="submit" value="Create">
                <a href="user_control.php">Cancel</a>
            </form>
        </td>
    </tr>
</table>
<?php
    require("includes/footer.php");
?>
