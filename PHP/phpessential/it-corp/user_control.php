<?php
    require_once("includes/session.php");
    confirm_logged_in();
    confirm_permission();
    require_once("includes/connection.php");
    require_once("includes/functions.php");

    include("includes/header.php");
    $delete = false;

    if(isset($_GET['u_d'])){
        if (intval($_GET['u_d']) == 1) {
            $delete = true;
        }
    }
?>
<link rel="stylesheet" href="css/user_control.css">
<table id="structure">
    <tr>
        <td id="navigation">
            <a href="staff.php">Return to Menu</a>
        </td>
        <td id="page">
            <?php
                if($delete){
                    echo "<p>The User was successfully deleted!</p>";
                }
            ?>
            <h2>Users of DO-iT Corp</h2>
            <h3>User:</h3>
            <ul>
                <?php
                    $user_set = get_all_users();
                    while ($users = mysql_fetch_array($user_set)) {
                        echo "<li><span><strong>";
                        echo $users['username'];
                        echo "</strong> (".$users['role'].")</span>";
                        echo "\t";
                        echo "<a href=\"edit_user.php?u_id=".$users['id']."\" data-userid=\"".$users['id']."\">Edit</a> \t";
                        if(!($users['role'] == "SUPERADMIN")){
                            echo "<a href=\"delete_user.php?u_id=".$users['id']."\">Delete</a>";
                        }
                        echo "</a>";
                        echo "</li>";
                    }

                ?>
            </ul>
            <p>
                <strong>+ <a href="new_user.php">Create a new User</a></strong>
            </p>
        </td>
    </tr>
</table>
<?php
    require("includes/footer.php");
?>
