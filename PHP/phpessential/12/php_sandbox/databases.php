<?php
    //1. Create a database connection
    $connection = mysql_connect("localhost", "prjdb_azubi", "nexus123");
    if (!$connection) {
        die("Database connection failed: " . mysql_error());
    }

    //2. Select a database to use
    $db_select = mysql_select_db("prjdb_azubi_02", $connection);
    if (!$db_select) {
        die("Database selection failed: " . mysql_error());
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Databases</title>
</head>
<body>

    <?php
        //3. Perfom database query
        $result = mysql_query("SELECT * FROM subjects", $connection);
        if (!$result) {
            die("Database query failed: " . mysql_error());
        }

        //4. User returned data
        while ($row = mysql_fetch_array($result)) {
            echo $row['menu_name']. " " . $row['position'] . "<br />";
        }
    ?>

</body>
</html>
<?php

    //5. Close connection
    mysql_close($connection);

?>