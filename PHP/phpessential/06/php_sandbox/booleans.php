<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Booleans and NULL</title>
</head>
<body>
	<?php
		$bool1 = true;
		$bool2 = false;

		echo '$bool1 : ' . $bool1 . "<br />";
		echo '$bool2 : ' . $bool2 . "<br />";
	?>
	<br />
	<?php
		$var1 = 3;
		$var2 = "cat";
		$var4 = 0;

		echo '$var1 is set : ' . isset($var1) . '<br/>';
		echo '$var2 is set : ' . isset($var2) . '<br/>';
		echo '$var3 is set : ' . isset($var3) . '<br/>';

		echo "<br />";

		unset($var1);
		echo '$var1 is set : ' . isset($var1) . '<br/>';
		echo '$var2 is set : ' . isset($var2) . '<br/>';
		echo '$var3 is set : ' . isset($var3) . '<br/>';

		echo "<br />";

		echo '$var1 is empty : ' . empty($var1) . '<br/>';
		echo '$var4 is null : ' . is_null($var4) . '<br/>';


	?>
</body>
</html>