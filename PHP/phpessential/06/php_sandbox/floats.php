<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Numbers: Floats</title>
</head>
<body>
	<?php
		$var1 = 3.14;
	?>
	<br/>
	Floating point: <?php echo $myFloat = 3.14; ?><br/>
	Round:  <?php echo round($myFloat, 1); ?><br/>
	Ceiling:  <?php echo ceil($myFloat); ?><br/>
	Floor:  <?php echo floor($myFloat); ?><br/>
</body>
</html>