<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Strings</title>
</head>
<body>
	<?php
		echo "Hello World <br />";
		echo 'Hello World! <br />';
		$my_variable = 'Hello World';
		echo $my_variable;
		echo $my_variable . "Again <br/>";
	 ?>
	 <br>
	 <?php
	 	echo "$my_variable Again. <br/>";
	 	echo "{$my_variable} Again. <br/>";
	 	echo "{$my_variable} Again. <br/>";
	 ?>
</body>
</html>