<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>String Functions</title>
</head>
<body>
	<?php
		$firstString = "The quick brown fox";
		$secondString  = " jumped over the lazy dog.";
	 ?>
	 <?php
	 	$thirString = $firstString;
	 	$thirString .= $secondString;

	 	echo $thirString;
	 ?>
	 <br>
	 Lowercase: <?php echo strtolower($thirString); ?><br>
	 Uppercase: <?php echo strtoupper($thirString); ?><br>
	 Uppercase first-letter: <?php echo ucfirst($thirString); ?><br>
	 Uppercase words: <?php echo ucwords($thirString); ?><br>
	 <br>
	 Length: <?php echo strlen($thirString); ?><br>
	 Trim: <?php echo $fourthString = $firstString . trim($secondString); ?><br>
	 Find: <?php echo strstr($thirString, "brown"); ?><br>
	 Replace by string: <?php echo str_replace("quick", "super-fast", $thirString); ?><br>
</body>
</html>