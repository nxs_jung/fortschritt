<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Type Casting</title>
</head>
<body>
	<?php
		$var1 = "2 brown foxes";
		$var2 = $var1 + 3;

		echo $var2 . "<br>";

		echo gettype($var1) . "<br>";
		echo gettype($var2) . "<br>";

		echo "<br>";

		settype($var2, "string");
		echo gettype($var2) . "<br>";

		echo "<br>";

		$var3 = (int) $var1;
		echo gettype($var3) . "<br>";

		echo "<br>";

		echo "array : " . is_array($var1) . "<br>";
		echo "bool : " . is_bool($var1) . "<br>";
		echo "float : " . is_float($var1) . "<br>";
		echo "int : " . is_int($var1) . "<br>";
		echo "null : " . is_null($var1) . "<br>";
		echo "numeric : " . is_numeric($var1) . "<br>";
		echo "string : " . is_string($var1) . "<br>";
	?>
</body>
</html>