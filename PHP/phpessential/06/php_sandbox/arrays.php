<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Arrays</title>
</head>
<body>
	<?php
		$array1 = array(4,8,15,16,23,42);
		echo $array1[1] . "<br />";
		$array2 = array(6, "fox", "dog", array("x","y","z"));
		echo $array2[3][1] . "<br />";
		$array2[3] = "cat";
		echo $array2[3] . "<br />";

		$array3 = array("first_name" => "Kevin", "last_name" => "Skoglund");
		echo $array3["first_name"] . " " . $array3["last_name"] . "<br />";

		$array3["first_name"] = "Larry";
		echo $array3["first_name"] . " " . $array3["last_name"];

	?>
	<pre>
		<?php
			print_r($array2);
		?>
	</pre>
</body>
</html>