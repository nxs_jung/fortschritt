<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Loops: foreach</title>
</head>
<body>
    <?php
        $ages = array(4, 8, 15, 16, 23, 42);

        foreach ($ages as $age) {
            echo $age . ", ";
        }

        //--------------------------------------------------------------//

        echo "<br />";
        echo "<br />";

        //--------------------------------------------------------------//

        foreach ($ages as $position => $age) {
            echo $position . ": " . $age . "<br />";
        }

        //--------------------------------------------------------------//

        echo "<br />";

        //--------------------------------------------------------------//

        $prices = array("Brand New Computer" => 2000, "1 month in Lynda.com Training Library" => 25, "Learning PHP" => "priceless");

        foreach ($prices as $key => $value) {
            if(is_int($value)){
                echo $key . ": $" . $value . "<br />";
            } else {
                echo $key . ": " .  $value . "<br />";
            }
        }
    ?>
</body>
</html>