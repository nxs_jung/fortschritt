<?php


for ($var = 0; $var <= 100; $var++ ){
    if ($var == 0) {
        echo $var . PHP_EOL;
    }else if($var % 15 == 0){
        echo "FizzBuzz " . PHP_EOL;
    }else if ($var % 5 == 0) {
        echo "Buzz" . PHP_EOL;
    }else if($var % 3 == 0){
        echo "Fizz" . PHP_EOL;
    }else{
        echo $var . PHP_EOL;
    }
}