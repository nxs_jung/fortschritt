<?php

require_once(LIB_PATH.DS."database.php");
require_once(LIB_PATH.DS."PHPMailer".DS."class.phpmailer.php");

class Comment extends DatabaseObject {

    protected static $table_name = "comments";
    protected static $db_fields = array('id', 'photograph_id', 'created', 'author', 'body');

    public $id;
    public $photograph_id;
    public $created;
    public $author;
    public $body;

    /**
     * Erstellt ein Comment-Objekt
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  int $photo_id die id des Fotos, welches kommentiert worden ist
     * @param  string $author   Der Autor des Kommentars
     * @param  string $body     Der Kommentar
     * @return Objekt/boolean
     */
    public static function make($photo_id, $author="Anymous" , $body=""){
        if(!empty($photo_id) && !empty($author) && !empty($body)){
            $comment = new Comment();
            $comment->photograph_id = (int) $photo_id;
            $comment->created = strftime("%Y-%m-%d %H:%M:%S", time());
            $comment->author = $author;
            $comment->body = $body;
            return $comment;
        }else{
            return false;
        }
    }

    /**
     * Gibt alle Comments für ein Foto zurück
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  integer $photo_id Die id des Fotos
     * @return Ressource
     */
    public static function find_comments_on($photo_id=0){
        global $database;

        $sql = "SELECT * FROM ".static::$table_name;
        $sql .= " WHERE photograph_id=".$database->escape_value($photo_id);
        $sql .= " ORDER BY created ASC";
        return static::find_by_sql($sql);
    }

    public function try_to_send_notification(){
        $mail = new PHPMailer();

        $mail->FromName = "Photo Gallery";
        $mail->From     = "jung@photogallery.com";
        $mail->addAddress("jung@nexus-netsoft.com", "Photo Gallery Admin");
        $mail->Subject  = "New Photo Gallery Comment";
        $created = datetime_to_text($this->created);
        $mail->Body     =<<<EMAILBODY

A new comment has been received in the Photo Gallery.

At {$created}, {$this->author} wrote:

    "{$this->body}"

If you want to see it, follow the link:
http://photogallery.php54.moj.nexus-developer.com/photo.php?id={$this->photograph_id}

EMAILBODY;
        // $mail->Body     = "A new comment has been received. \n";
        // $mail->Body     .= "To this photo by ".$this->author.": \n";
        // $mail->Body     .= "http://photogallery.php54.moj.nexus-developer.com/photo.php?id=".$this->photograph_id;

        $result = $mail->send();
        return $result;
    }

}