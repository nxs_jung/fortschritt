<?php

require_once(LIB_PATH.DS."database.php");
require_once(LIB_PATH.DS."database_object.php");

class Photograph extends DatabaseObject {

    protected static $table_name = 'photographs';
    protected static $db_fields = array('id','filename', 'type', 'size', 'caption');

    public $id;
    public $filename ;
    public $type;
    public $size;
    public $caption;

    public $temp_path;
    protected $upload_dir = "img";
    public $target_path;
    public $errors = array();
    private $validator;

    function __construct() {
        $this->validator = new Validator($this);
    }


    // Pass in $_FILE(['uploaded_file']) as an argument
    /**
     * Ließt die Werte der hochgeladenen Datei aus und setzt die Attribute
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  Array $file File-Array mit den Parametern/Attributen des hochgeladenen Files
     * @return void/boolean
     */
    public function attach_file($file){
        $this->errors = $this->validator->validateUp($file);
        // Perform error checking on the form parameters
        if(count($this->errors) > 1){
            return false;
        }else{
            // Set object attributes to the form parameters.
            $this->temp_path    = $file['tmp_name'];
            $this->filename     = basename($file['name']);
            $this->type         = $file['type'];
            $this->size         = $file['size'];
            // Don't worry about saving anything to the database yet
            return true;
        }
    }

    /**
     * Überschreibt die default save-Methode und bewegt bei Erfolg die Datei aus dem tmp in das richtige Verzeichnis
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return void
     */
    public function save(){
        if(isset($this->id)){
            // Really just to update the caption
            $this->update();
        }else{
            // Make sure there are no errors
            // Can't save if there are pre-existing errors
            if(!empty($this->errors)){return false;}

            $this->target_path = SITE_ROOT.DS.'public'.DS. $this->upload_dir.DS.$this->filename;

            $this->errors = $this->validator->validate();

            if(count($this->errors) >= 1){return false;}

            // Attempt to move the file
            if(move_uploaded_file($this->temp_path, $this->target_path)){
                // Success
                // Save a corresponding etry to the database
                if($this->create()){
                    // We are done with temp_path, the file isn't there anymore
                    unset($this->temp_path);
                    return true;
                }
            }else{
                // File was not moved
                $this->errors[] = "The file upload failed, possibly due to incorrect permissions on the upload folder.";
                return false;
            }
        }
    }

    /**
     * Löscht ein Bild aus der Datenbank und aus dem Verzeichnis
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return void/boolean
     */
    public function destroy(){
        // First remove the database entry
        if($this->delete()){
            // then remove the file
            $target_path = SITE_ROOT.DS.'public'.DS.$this->image_path();
            return unlink($target_path) ? true : false;
        }else{
            // database delete failes
            return false;
        }
    }

    /**
     * Gibt den Path für die Bilder zurück
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return String
     */
    public function image_path(){
        return $this->upload_dir.DS.$this->filename;
    }

    /**
     * wandelt die Dateigröße in Text um (bytes, KB, MB)
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return String
     */
    public function size_as_text(){
        if($this->size < 1024){
            return $this->size." bytes";
        }elseif ($this->size < 1048576) {
            $size_kb = round($this->size/1024);
            return $size_kb." KB";
        }else{
            $size_mb = round($this->size/1048576, 1);
            return $size_mb." MB";
        }
    }

    /**
     * Gibt alle Comments auf das Foto an
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return Ressource
     */
    public function comments(){
        return Comment::find_comments_on($this->id);
    }
}