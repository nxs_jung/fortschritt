<?php

class User_Validator extends Validator {

    public function __construct(User $object) {
        $this->object = $object;
    }

    public function validate(Array $post = null){
        $this->validate_U($post);
        return $this->occured_errors;
    }


    private function validate_U($post){
        // Daten die anders behandelt werden müssen aussondern
        $aUsern = array_slice($post, array_search("username", array_keys($post)), 1);
        $aMail = array_slice($post, array_search("mail", array_keys($post)), 1);
        $aVideo = array_slice($post, array_search("create_vid", array_keys($post)), 1);
        $aPhoto = array_slice($post, array_search("create_pho", array_keys($post)), 1);
        $aUser = array_slice($post, array_search("create_user", array_keys($post)), 1);

        // Überschüssige Daten aus dem POST-Array löschen/unseten
        unset($post['submit']);

        unset($post[array_keys($aUsern)[array_search("username", array_keys($aUsern))]]);
        unset($post[array_keys($aMail)[array_search("mail", array_keys($aMail))]]);

        unset($post[array_keys($aVideo)[array_search("create_vid", array_keys($aVideo))]]);
        unset($post[array_keys($aPhoto)[array_search("create_pho", array_keys($aPhoto))]]);
        unset($post[array_keys($aUser)[array_search("create_user", array_keys($aUser))]]);

        foreach ($post as $name => $value) {
            if(isset($value) && $value !== ""){
                $this->object->$name = $value;
            }else{
                $this->occured_errors[] = $this->errors[$name];
            }
        }

        if(isset($aUsern['username']) && $aUsern['username'] !== "" && User::find_by_username($aUsern['username']) === NULL){
            $this->object->username = $aUsern['username'];
        }else{
            $this->occured_errors[] = $this->errors['username'];
        }

        $this->object->set_role($aVideo['create_vid'], $aPhoto['create_pho'], $aUser['create_user']);

        if (isset($aMail['mail']) && $aMail['mail'] !== '' && !$this->object->set_mail($aMail['mail'])) {
            $this->occured_errors[] = $this->errors["mail"];
        }elseif(!isset($aMail['mail']) || $aMail['mail'] == ''){
            $this->occured_errors[] = $this->errors["no_mail"];
        }else{
            $this->object->set_mail($aMail['mail']);
        }

    }

}