<?php

require_once(LIB_PATH.DS."database.php");
require_once(LIB_PATH.DS."database_object.php");
require_once(LIB_PATH.DS.'PHPMailer'.DS.'class.phpmailer.php');
require_once(LIB_PATH.DS.'PHPMailer'.DS.'class.smtp.php');
require_once(LIB_PATH.DS.'PHPMailer'.DS.'language'.DS.'phpmailer.lang-de.php');

class User extends DatabaseObject {

    protected static $table_name = 'users';
    protected static $db_fields = array(
            'id',
            'username',
            'password',
            'first_name',
            'last_name',
            'mail',
            'role',
            'admin'
            );

    public $id;
    public $username;
    public $password;
    public $first_name;
    public $last_name;
    public $mail;
    public $role;
    public $admin;

    public $rRights;


    /**
     * Authentifiziert einen User mit username und passwort, indem eine Datenbankabfrage gestartet wird
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  string $username Username des Benutzers
     * @param  string $password Password des Benutzers
     * @return Array/boolean
     */
    public static function authenticate($username = "", $password = ""){
        global $database;

        $username = $database->escape_value($username);
        $password = $database->escape_value($password);

        $sql = "SELECT * FROM ".static::$table_name." ";
        $sql .= "WHERE username = '".$username."' ";
        $sql .= "AND password = '".$password."' ";
        $sql .= "LIMIT 1 ";

        $result_array = static::find_by_sql($sql);
        return (!empty($result_array)) ? array_shift($result_array) : false ;
    }

    /**
     * Checks the hash, if it is correct
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  int    $id       the user-id
     * @param  String $hash     the hash for the pw-reset
     * @return boolean
     */
    public static function check_hash($id, $hash){

        return ((sha1(HASH."".$id)) === $hash);
    }

    public static function forgotten_password($username, $mail_adress){
        global $database;
        $mail = new PHPMailer();
        $errors = array();

        $user = User::find_by_username($username);
        if($user instanceOf User){
            $errors = $user->validate_mail($mail_adress);


            if(($user->mail === $mail_adress) && (count($errors) < 1)){
                $mail->FromName = "Photo Gallery";
                $mail->From     = "jung@photogallery.com";
                $mail->addAddress($user->mail."", $user->full_name()."");
                $mail->Subject  = "Forgotten Password";
                $mail->Body     = "Hey ".$user->full_name().", \n";
                $mail->Body     .= "Do you forgot your password? \n";
                $mail->Body     .= "Follow this link to reset your password:\n \n ";
                $mail->Body     .= "\t http://photogallery.php54.moj.nexus-developer.com/reset_pw.php?h=".$user->generate_hash()."|".$user->id;

                $result = $mail->send();
                return $result;
            }else{
                $errors[] = "The Mail was not equal to your account mail.";
                return $errors;
            }
        }else{
            return "The Username was incorrect.";
        }


    }

    public function init_role(){
        $this->rRights = new Role();
        $this->rRights->bezeichnung = $this->role;
        $this->rRights->init_role();
    }


    public function authorize_admin(){
        return $this->admin;
    }

    /**
     * Gibt den kompletten Namen des Users zurück - 'Vorname' 'Nachname'
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return string
     */
    public function full_name(){
        if(isset($this->first_name)&& isset($this->last_name)){
            return $this->first_name . " " . $this->last_name;
        }else{
            return "";
        }
    }

    public function set_mail($mail){
        $errors = $this->validate_mail($mail);
        if(count($errors) < 1 && $this->find_by_mail($mail) === NULL){
            $this->mail = $mail;
            return true;
        }else{
            return false;
        }
    }

    public function set_role($pho, $vid, $user){
        $this->rRights = $this->rRights->set_role($pho, $vid, $user);
        $this->role = $this->rRights->bezeichnung;
        if($this->role){
            return "Die Rolle wurde erfolgreich geändert.";
        }else{
            return "Ein Fehler ist aufgetreten. ";
        }
    }

    /**
     * validates the mail to be changed
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  String $mail mail-adress for changing the mail
     * @return Array
     */
    public function validate_mail($mail){
        $mail_errors = array();

        //http://php.net/manual/de/filter.filters.validate.php

        if(!filter_var($mail, FILTER_VALIDATE_EMAIL)){
            $mail_errors[] = "Mail: The eMail-Adress ".$mail." was incorrect!";
        }

        return $mail_errors;
    }

    public static function find_by_username($username){
        global $database;

        $sql = "SELECT * FROM ".static::$table_name." WHERE username='".$database->escape_value($username)."' LIMIT 1";
        $user_set = static::find_by_sql($sql);

        $user = array_shift($user_set);

        if($user){
            return $user;
        }else{
            return NULL;
        }
    }

    public static function find_by_mail($mail){
        global $database;

        $sql = "SELECT * FROM ".static::$table_name." WHERE mail='".$database->escape_value($mail)."' LIMIT 1";
        $user = static::find_by_sql($sql);

        if($user){
            return $user;
        }else{
            return NULL;
        }
    }

    /**
     * Es erstellt einen neuen User in der Datenbank
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return void
     */
    protected function create(){
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        array_shift($attributes);

        $sql = "INSERT INTO ".static::$table_name." (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";

        if($database->query($sql)){
            $this->id = $database->insert_id();
            return true;
        }else{
            return false;
        }
    }

    private function generate_hash(){
        return sha1(HASH."".$this->id);
    }
}