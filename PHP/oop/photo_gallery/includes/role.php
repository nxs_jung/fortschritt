<?php

class Role extends DatabaseObject {


    protected static $table_name = 'role';
    protected static $db_fields = array(
            'bezeichnung',
            'photo',
            'video',
            'user'
            );

    public $bezeichnung;
    public $photo;
    public $video;
    public $user;

    public function init_role(){
        $settings = $this->find_by_bezeichnung();
        foreach ($settings as $key => $wert) {
            if(gettype($key) === "string" ){
                $this->$key = $wert;
            }
        }
    }


    public function authorize_video(){
        return $this->find_key_by_bezeichnung("video");
    }

    public function authorize_photo(){
        return $this->find_key_by_bezeichnung("photo");
    }

    public function authorize_user(){
        return $this->find_key_by_bezeichnung("user");
    }

    public function find_by_bezeichnung(){
        global $database;
        $sql = "SELECT * FROM ".static::$table_name." WHERE bezeichnung='".$this->bezeichnung."' LIMIT 1";

        $result_array = $database->fetch_array($database->query($sql));
        return $result_array;
    }

    private function find_key_by_bezeichnung($key){
        global $database;
        $sql = "SELECT ".$key." FROM ".static::$table_name." WHERE bezeichnung='".$this->bezeichnung."' LIMIT 1";

        $result_array = $database->fetch_array($database->query($sql));
        return (!empty($result_array)) ? array_shift($result_array) : false ;
    }

    public function set_role($pho, $vid, $user){
        global $database;
        if(isset($pho) && isset($vid) && isset($user)){
            $sql = "SELECT * FROM ".static::$table_name." ";
            $sql .= "WHERE photo ='".$pho;
            $sql .= "' AND video ='".$vid;
            $sql .= "' AND user ='".$user;
            $sql .= "' LIMIT 1";

            $result_array = static::find_by_sql($sql);
            return (!empty($result_array)) ? array_shift($result_array) : false ;
        }else{
            return false;
        }
    }
}