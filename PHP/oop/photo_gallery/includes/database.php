<?php

require_once(LIB_PATH.DS."config.php");

class MySQLDatabase{
    private $connection;
    private $magic_quotes_active;
    private $mysql_real_escape_string_exists;
    public $last_query;

    /**
     * Überprüft ob magic_quotes aktiv sind und ob real_escape_string existieren, danach wird die Verbindung geöffnet
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     */
    public function __construct(){
        $this->magic_quotes_active = get_magic_quotes_gpc();
        $this->mysql_real_escape_string_exists = function_exists("mysql_real_escape_string");
        $this->open_connection();
    }

    /**
     * Startet die Verbindung mit der Datenbank, welche in der config.php hinterlegt ist
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return void
     */
    public function open_connection(){

        $this->connection = mysql_connect(DB_SERVER, DB_USER, DB_PASS);
        if(!$this->connection){
            die("Database connection failed: " . mysql_error());
        }else{
            $db_select = mysql_select_db(DB_NAME, $this->connection);
            if(!$db_select){
                die("Database selection failed: " . mysql_error());
            }
        }
    }

    /**
     * Beendet die Verbindung mit der Datenbank
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return void
     */
    public function close_connection(){
        if(isset($this->connection)){
            mysql_close($this->connection);
            unset($this->connection);
        }
    }

    /**
     * Führt eine SQL-Query aus die übergeben wird
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  String $sql auszuführende Query
     * @return Ressource/boolean
     */
    public function query($sql){
        $this->last_query = $sql;
        $result = mysql_query($sql, $this->connection);
        $this->confirm_query($result);
        return $result;
    }


    /**
     * Escaped alle Sonderzeichen in einem String und macht es "Datenbankfreundlich"
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  String $value ein Wert oder eine Query die auf die Datenbank angewendet werden soll
     * @return String
     */
    public function escape_value($value){

        if ($this->mysql_real_escape_string_exists) {
            if ($this->magic_quotes_active) {
                $value = stripslashes($value);
            }
            $value = mysql_real_escape_string($value);
        }else{
            if (!$this->magic_quotes_active) {
                $value = addslashes($value);
            }
        }
        return $value;
    }

    // "database neutral" methods
    /**
     * Zählt die Zeilen eines Ressources - für spätere usability ausgelagert
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  Ressource $result_set
     * @return Int
     */
    public function num_rows($result_set){
        return mysql_num_rows($result_set);
    }

    /**
     * Gibt die ID zurück die zuletzt hinzugefügt worden ist
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return Int
     */
    public function insert_id(){
        return mysql_insert_id($this->connection);
    }

    /**
     * Gibt die Anzahl der Zeilen wieder, die beim letzten Query verändert wurden
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return Int
     */
    public function affected_rows(){
        return mysql_affected_rows($this->connection);
    }

    /**
     * Zieht aus einem Ressource ein Array
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  Ressource $result_set
     * @return Array
     */
    public function fetch_array($result_set){
        return mysql_fetch_array($result_set);
    }

    /* PRIVAT METHODS */
    /**
     * Überprüft eine Query, ob Sie erfolgreich war und gibt ggf. einen Fehlermeldung zurück
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  Ressource/boolean $result [description]
     * @return [type]         [description]
     */
    private function confirm_query($result){
        if(!$result){
            $output = "Database query failed: " . mysql_error()."<br>";
            $output .= "Last SQL query: " . $this->last_query;
            die($output);
        }
    }

}

$database = new MySQLDatabase();