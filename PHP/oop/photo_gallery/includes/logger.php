<?php

require_once(LIB_PATH.DS."config.php");

class Logger{

    public $log_file;

    /**
     * Erstellt einen Logger
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     */
    public function __construct(){
        $this->log_file = LOG_FILE;
    }

    /**
     * Loggt eine action in der log-Datei mit Statusnachricht
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  string $action  die ausgeführte Aktion (Login, Logout, etc.)
     * @param  string $message eine Statusnachricht (wer z.b. etwas gemacht hat, oder was gelöscht wurde)
     * @return void/String
     */
    public function log_action($action, $message=""){
        $this->move_to_log();

        if(file_exists($this->log_file) && is_writable($this->log_file) ){
            $handle = fopen($this->log_file, 'a');
        }elseif(file_exists($this->log_file) ){
            echo "ERROR: File is not writeable";
        }else{
            $handle = fopen($this->log_file, 'w');
        }

        $log = strftime("%Y-%m-%d %H:%M:%S", time()) . " | ";
        $log .= "".$action.": ";
        $log .= $message;
        $log .= "\n";

        fwrite($handle, $log);
        fclose($handle);
    }

    /**
     * ließt die log-Datei aus
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return String
     */
    public function read_log(){
        $this->move_to_log();

        if($content = file_get_contents($this->log_file)){
            return nl2br($content);
        }else{
            return "No log avaible!";
        }
    }


    /**
     * überschreibt die log-Datei mit einer "Cleared-By"-Zeile
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  int $id User-ID, der die log-Datei cleared
     * @return String
     */
    public function clear_log($id){
        $this->move_to_log();

        unlink($this->log_file);
        $id = (int) $id;
        $this->log_action("Clear", "UserID ". $id ."| The log was cleared");
        return "CLEARED!";
    }


    /**
     * Bewegt den Zeiger in das log-Verzeichnis
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return void
     */
    private function move_to_log(){
        chdir(LOG_DIR);
    }
}

$logger = new Logger();