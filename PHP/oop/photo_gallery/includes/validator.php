<?php

class Validator {

    private $object = null;

    protected $errors = array(
        "caption"               => "The caption can only be 255 characters long.",
        "filename"              => "The file location was not available",
        "target_path"           => "The file already exists.",
        "username"              => "The username allready existst.",
        "password"              => "The password was not set.",
        "first_name"            => "The First Name was not set.",
        "last_name"             => "The Last name was not set.",
        "mail"                  => "The email allready exists or is incorrect.",
        "no_mail"               => "The Mail-address was not set.",
        UPLOAD_ERR_OK           => "No errors.",
        UPLOAD_ERR_INI_SIZE     => "Larger than upload_max_filesize.",
        UPLOAD_ERR_FORM_SIZE    => "Larger than form MAX_FILE_SIZE.",
        UPLOAD_ERR_PARTIAL      => "Partial upload.",
        UPLOAD_ERR_NO_FILE      => "No file.",
        UPLOAD_ERR_NO_TMP_DIR   => "No temporary directory.",
        UPLOAD_ERR_CANT_WRITE   => "Can't write to disk.",
        UPLOAD_ERR_EXTENSION    => "File upload stopped by extension.",
        );

    public $occured_errors = array();

    public function __construct(Photograph $object) {
        $this->object = $object;
    }

    public function validate(Array $post = null){
        $this->validate_PV();
        return $this->occured_errors;
    }

    public function validateUp($file){
        $error=array();
        if(!$file || empty($file) || !is_array($file)){
            // error: nothing uploaded or wrong argument usage
            $error[] = "No file was uploaded.";
        }elseif ($file['error'] !== 0) {
            $error[] = $this->errors[$file['errors']];
        }
        return $error;
    }

    private function validate_PV(){
        // Make sure the caption is not too long for the DB
        if(strlen($this->object->caption)> 255){
            $this->occured_errors[] = $this->errors["caption"];
        }

        // Can't save without filename and temp location
        if(empty($this->object->filename)||empty($this->object->temp_path)){
            $this->occured_errors[] = $this->errors["filename"];
        }

        // Make sure a file doesn't already exist in the target location
        if(file_exists($this->object->target_path)){
            $this->occured_errors[] = $this->errors["target_path"];
        }
    }

}