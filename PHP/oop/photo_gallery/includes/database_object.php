<?php

require_once(LIB_PATH.DS."database.php");

abstract class DatabaseObject {

    protected static $table_name;
    protected static $db_fields;

    // Common DB Methods
    /**
     * Gibt alle Ergebnisse für eine Tabelle wieder, die statisch definiert ist
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return Array
     */
    public static function find_all(){
        $query = "SELECT * FROM ".static::$table_name;
        return static::find_by_sql($query);
    }

    /**
     * Gibt eine Zeile zurück in Abhängigkeit von der ID
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  integer $id
     * @return Array
     */
    public static function find_by_id($id = 0){
        global $database;
        $query = "SELECT * FROM ".static::$table_name." WHERE id=".$database->escape_value($id)." LIMIT 1";
        $result_array = static::find_by_sql($query);
        return !empty($result_array) ? array_shift($result_array): false ;
    }

    /**
     * Durchsucht die Datenbank mit einer SQL Query, gibt ein Array zurück
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  string $sql die Suchbedingung
     * @return Array
     */
    public static function find_by_sql($sql=''){
        global $database;
        $result_set = $database->query($sql);
        $object_array = array();
        while ($row = $database->fetch_array($result_set)) {
            $object_array[] = static::instantiate($row);
        }
        return $object_array;
    }

    public static function count_all(){
        global $database;

        $sql = "SELECT COUNT(*) FROM ".static::$table_name;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }

    /**
     * Instanziiert ein Object aus einem Array
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  Array $record Attribut/Parameter Array
     * @return Object
     */
    private static function instantiate($record){
        $object = new static;

        foreach ($record as $attribute => $value) {
            if($object->has_attribute($attribute)){
                $object->$attribute = $value;
            }
        }

        return $object;
    }

    /**
     * Überprüft ob eine Klasse ein gewisses Attribut/Parameter besitzt
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  String  $attribute
     * @return boolean
     */
    private function has_attribute($attribute){
        $object_vars = $this->attributes();
        return array_key_exists($attribute, $object_vars);
    }

    // ------------------------------------- CRUD ------------------------------------- //

    /**
     * Filtert die Attribute eines Objekts mit ihrem Wert
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return Array
     */
    protected function attributes(){
        // return an array of attributes names and their values
        $attributes = array();
        foreach (static::$db_fields as $field) {
            if(property_exists($this, $field)){
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    /**
     * Escaped die Werte eines Attributs-Arrays
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return Array
     */
    protected function sanitized_attributes(){
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    /**
     * Es wird überprüft, ob eine ID vorhanden ist, wenn nein wird der User neu erstellt
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return void
     */
    public function save(){
        // A new record won't have an id yet
        return isset($this->id) ? $this->update() : $this->create();
    }

    /**
     * Es erstellt einen neuen User in der Datenbank
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return void
     */
    protected function create(){
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();

        $sql = "INSERT INTO ".static::$table_name." (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";

        if($database->query($sql)){
            $this->id = $database->insert_id();
            return true;
        }else{
            return false;
        }
    }

    /**
     * Updated eine Zeile in der Datenbank für ein Objekt
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return boolean/void
     */
    protected function update(){
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();

        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "".$key."='".$value."'";
        }

        $sql = "UPDATE ".static::$table_name." SET ";
        $sql .= join(", ", $attribute_pairs);
        $sql .= " WHERE id=".$database->escape_value($this->id);
        $database->query($sql);
        return ($database->affected_rows() == 1) ? true : false;
    }

    /**
     * Löscht einen Eintrag/eine Zeile/ein Objekt aus der Datenbank
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return boolean/void
     */
    public function delete(){
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - DELETE FROM table WHERE condition LIMIT 1
        // - escape all values to prevent SQL injection
        // - use LIMIT 1
        $sql = "DELETE FROM ".static::$table_name;
        $sql .= " WHERE id=".$database->escape_value($this->id);
        $sql .= " LIMIT 1";
        $database->query($sql);
        return ($database->affected_rows() == 1) ? true : false;
    }

}


