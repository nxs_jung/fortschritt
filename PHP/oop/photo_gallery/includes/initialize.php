<?php
// Define the core paths
// Define them as absolute paths to make sure that require_once works as expected

error_reporting( E_ALL^E_NOTICE );
ini_set('display_errors', 1);

// DIRECTORY_SEPARATOR is a PHP pre-defined constant
// (\ for windows, / for Unix)
defined('DS') ? NULL : define('DS', DIRECTORY_SEPARATOR);

/**
 * definiert den "Seiten-Root" unabhängig von dem System mit dirname(__DIR__)
 */
defined('SITE_ROOT')    ? null
                        : define('SITE_ROOT', dirname(__DIR__));

                        // /var/www/moj/htdocs/oop/photo_gallery/

/**
 * Definiert den "Include/Liberary-Path" unabhängig von dem System mit __DIR__
 */
defined('LIB_PATH') ? null
                    : define('LIB_PATH', __DIR__);

                        // /var/www/moj/htdocs/oop/photo_gallery/includes

/**
 * Definiert das LOG-Verzeichnis
 */
defined('LOG_DIR')  ? null
                    : define('LOG_DIR', SITE_ROOT.DS.'logs');


/**
 * Included die benötigten Dateien.
 * Reihenfolge ist WICHTIG!
 */
// Zuerst wird die config-Datei includiert
require_once(LIB_PATH.DS."config.php");

// Dann die Funktionen
require_once(LIB_PATH.DS."functions.php");

// Darauf folgt die Session, dann die Datenbank, dann die Datenbank-Objekte - da sie die Datenbank-Klasse benötigt
require_once(LIB_PATH.DS."session.php");
require_once(LIB_PATH.DS."database.php");
require_once(LIB_PATH.DS."database_object.php");
require_once(LIB_PATH.DS."pagination.php");
require_once(LIB_PATH.DS."validator.php");
require_once(LIB_PATH.DS."validator_user.php");
require_once(LIB_PATH.DS."PHPMailer".DS."class.phpmailer.php");
require_once(LIB_PATH.DS."PHPMailer".DS."class.smtp.php");
require_once(LIB_PATH.DS."PHPMailer".DS."language".DS."phpmailer.lang-de.php");

// Am Ende wird der Rest hinzugefügt, der die Session, Datenbank und Datenbank-Objekt Klasse benötigt
require_once(LIB_PATH.DS."user.php");
require_once(LIB_PATH.DS."photograph.php");
require_once(LIB_PATH.DS."videos.php");
require_once(LIB_PATH.DS."comment.php");
require_once(LIB_PATH.DS."logger.php");