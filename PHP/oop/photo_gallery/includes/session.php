<?php
// A class to help work with Sessions
// In our case, primarily to manage logging users in and out
//
// Keep in mind when workin with sessions that it is generally
// inadvisible to store DB-related objects in sessions

class Session {

    private $logged_in = false;
    private $user;
    public $user_id;
    public $message;
    public $role;

    /**
     * Startet die Session und überprüft, ob der Nutzer eingelogt ist
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     */
    public function __construct(){
        session_start();
        $this->check_message();
        $this->check_login();
        //if($this->logged_in){
        //    $this->user = User::find_by_id($this->user_id);
        //}
        //else{
        //     // actions to take right away if user is not logged in
        // }
    }

    public function init(){
        $this->user = User::find_by_id($this->user_id);
        $this->role = new Role();
        $this->role->bezeichnung = $this->user->role;
        $this->role->init_role();
    }

    /**
     * Gibt den logged_in - Status zurück
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return boolean [description]
     */
    public function is_logged_in(){
        return $this->logged_in;
    }

    /**
     * Logt den User ein und setzt die Session und den ID-Parameter
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  User   $user
     * @return void
     */
    public function login(User $user){
        // database should find user based on username/password
        $this->user_id = $_SESSION['user_id'] = $user->id;
        $this->logged_in = true;
    }

    /**
     * Logt den User aus und unetted die Session und den ID-Parameter
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return [type] [description]
     */
    public function logout(){
        unset($_SESSION['user_id']);
        unset($this->user_id);
        $this->logged_in = false;
    }

    /**
     * Gibt entweder die Session-Message wieder oder setzt sie neu
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @param  string $msg Session-Nachricht, die auf einer nächsten Seite angegeben wird
     * @return String/void
     */
    public function message($msg=""){
        if(!empty($msg)){
            // then this is "set message"
            // make sure your understand why $this->message = $msg wouldn't work
            $_SESSION['message'] = $msg;
        }else{
            // then this is "get message"
            return $this->message;
        }
    }

    /**
     * Überprüft den logged_in - Status des Nutzers
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return void
     */
    private function check_login(){
        if(isset($_SESSION['user_id'])){
            $this->user_id = $_SESSION['user_id'];
            $this->logged_in = true;
        }else{
            unset($this->user_id);
            $this->logged_in = false;
        }
    }

    /**
     * Überprüft, ob eine $_SESSION-Message gesetzt ist und übernimmt sie in die Session-Msg
     *
     * @author Moritz Jung <jung@nexus-netsoft.com>
     * @return void
     */
    private function check_message(){
        if(isset($_SESSION['message'])){
            $this->message = $_SESSION['message'];
            unset($_SESSION['message']);
        }else{
            $this->message = "";
        }
    }
}

$session = new Session();
$message = $session->message();