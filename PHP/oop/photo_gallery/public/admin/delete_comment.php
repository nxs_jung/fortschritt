<?php
require_once("../../includes/initialize.php");

if(!$session->is_logged_in()){ redirect_to("login.php");}

if(!isset($_GET['id'])){
    $session->message("No comment ID was provided.");
    redirect_to("list_photos.php");
}

$comment = Comment::find_by_id($_GET['pho_id']);
if($comment && $comment->delete()){
    $logger->log_action("Delete", "UserID ". $session->user_id ." | UserID ".$session->user_id." deleted a comment!");
    $session->message("The comment was deleted.");
    redirect_to("comments.php?id=".$comment->photograph_id);
}else{
    $session->message("The comment could not be deleted.");
    redirect_to('list_photos.php');
}

if(isset($database)){
    $database->close_connection();
}