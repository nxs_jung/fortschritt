<?php
require_once("../../includes/initialize.php");

if(!$session->is_logged_in()){
    redirect_to("login.php");
}

$max_file_size = 10485760;   // expressed in bytes
                             //    10240 =  10 KB
                             //   102400 = 100 KB
                             //  1048576 =   1 MB
                             // 10485760 =  10 MB

if(isset($_POST['submit'])){
    $video = new Video();
    $video->caption = $_POST['caption'];
    $video->attach_file($_FILES['file_upload']);
    if($video->save()){
        $logger->log_action("Upload", "UserID ". $session->user_id ."| UserID ".$session->user_id." uploaded a file!");
        $session->message("Video upload successfully.");
        redirect_to('list_videos.php');
    }else{
        $message = join("<br />", $video->errors);
    }
}

include_layout_template('admin_header.php');
?>

<h2>Video Upload</h2>
<?php if(isset($message)){echo output_message($message);}?>

<form action="video_upload.php" enctype="multipart/form-data" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $max_file_size;?>" />
    <p><input type="file" name="file_upload" /></p>
    <p>Caption<input type="text" name="caption" value="" /></p>
    <input type="submit" name="submit" value="Upload" />
</form>

<?php
include_layout_template('admin_footer.php');
?>