<?php
require_once("../../includes/initialize.php");

if(!$session->is_logged_in()){ redirect_to("login.php");}

if(!isset($_GET['id'])){
    $session->message("No photograph ID was provided.");
    redirect_to("list_photos.php");
}

$photo = Photograph::find_by_id($_GET['id']);
if($photo && $photo->destroy()){
    $logger->log_action("Delete", "UserID ". $session->user_id ."| UserID ".$session->user_id." deleted a file (".
        $photo->filename.") !");
    $session->message("The photo was deleted.");
    redirect_to('list_photos.php');
}else{
    $session->message("The photo could not be deleted.");
    redirect_to('list_photos.php');
}

if(isset($database)){
    $database->close_connection();
}