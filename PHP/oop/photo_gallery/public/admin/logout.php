<?php
require_once("../../includes/initialize.php");

if(!$session-> is_logged_in()){
    echo "".$session->is_logged_in();
    redirect_to("index.php");
}

$logger->log_action("Logout", "UserID ". $session->user_id ."| UserID ".$session->user_id." logged out!");
$session->logout();

include_layout_template('admin_header.php');
?>

<h2>Staff Menu: Log out</h2>

<a href="login.php">Login</a>
<a href="../index.php">Public Seite</a>


<?php
include_layout_template('admin_footer.php');
?>