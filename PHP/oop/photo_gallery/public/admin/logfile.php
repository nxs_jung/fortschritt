<?php
require_once("../../includes/initialize.php");

if(!$session-> is_logged_in()){
    echo "".$session->is_logged_in();
    redirect_to("index.php");
}

if($_GET['clear'] == 'true'){
    $html_log = $logger->clear_log($_SESSION['user_id']);
    redirect_to('logfile.php');
}else{
    $html_log = $logger->read_log();
    $html_display_log = $logger->read_log_html();
}

include_layout_template('admin_header.php');
?>

<h2>Staff Menu: Log File</h2>
<?php if(isset($message)){echo output_message($message);}?>

<a href="index.php">Zur&uuml;ck</a>

<div class="log">
    <?php
    echo $html_log;
    ?>
</div>

<form action="logfile.php" method="GET">
    <button name="clear" value="true">Clear!</button>
</form>


<?php
include_layout_template('admin_footer.php');
?>