<?php
require_once("../../includes/initialize.php");

if(!$session->is_logged_in()){
    redirect_to("login.php");
}
$session->init();
if(!$session->role->authorize_video()){
    redirect_to("index.php");
}

// 1. the current page number ($current_page)
$page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;

// 2. records per page ($per_page)
$per_page = 3;

// 3. total record count ($total_count)
$total_count = Video::count_all();

// Find all videos
// use pagination instead
// $videos = Photograph::find_all();

$pagination = new Pagination($page, $per_page, $total_count);

// Instead if findíng all records, just find the records for this page
$sql = "SELECT * FROM videos ";
$sql .= " LIMIT ".$per_page;
$sql .= " OFFSET ".$pagination->offset();
$videos = Video::find_by_sql($sql);

// Need to add ?page=$page to all links we want to
// maintain the current page (or store $page in $session)

$aUser = User::find_by_id($session->user_id);

include_layout_template('admin_header.php');
?>

<h2>Photographs</h2>
<?php if(isset($message)){echo output_message($message);}?>

<table class="bordered">
    <tr>
        <th>Video</th>
        <th>Filename</th>
        <th>Caption</th>
        <th>Size</th>
        <th>Type</th>
        <th>Comments</th>
        <?php if($aUser->authorize_admin()){ ?>
            <th>&nbsp;</th>
        <?php } ?>
    </tr>
    <?php foreach ($videos as $video): ?>
    <tr>
        <td><video src="../<?php echo $video->image_path();?>" width="100" /></td>
        <td><?php echo $video->filename;?></td>
        <td><?php echo $video->caption;?></td>
        <td><?php echo $video->size_as_text();?></td>
        <td><?php echo $video->type;?></td>
        <td>
            <a href="comments.php?vid_id=<?php echo $video->id;?>">
                <?php echo count($video->comments());?>
            </a>
        </td>
        <?php if($aUser->authorize_admin()){ ?>
            <td><a href="delete_video.php?id=<?php echo $video->id;?>">Delete</a></td>
        <?php } ?>
    </tr>
    <?php endforeach; ?>
</table>

<div id="pagination" style="clear: both;">
    <?php
        if($pagination->total_pages() > 1){
            if($pagination->has_previous_page()){
                echo "<a href=\"list_videos.php?page=";
                echo $pagination->previous_page();
                echo "\">&laquo; Previous</a>   ";
            }

            for($i = 1; $i <= $pagination->total_pages(); $i++){
                if($i == $page){
                    echo "<span class=\"selected\">".$i."</span> ";
                }else{
                    echo " <a href=\"list_videos.php?page=".$i."\">".$i."</a> ";
                }
            }

            if($pagination->has_next_page()){
                echo "   <a href=\"list_videos.php?page=";
                echo $pagination->next_page();
                echo "\">Next &raquo;</a>";
            }
        }
    ?>
</div>

<br />
<a href="video_upload.php">Upload a new photograph</a><br />
<a href="index.php">Zur&uuml;ck</a>

<?php include_layout_template('admin_footer.php') ?>