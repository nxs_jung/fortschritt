<?php
require_once("../../includes/initialize.php");

if($session-> is_logged_in()){
    redirect_to("index.php");
}

// Remember to give your form's submit tag a name="submit" attribute!
if(isset($_POST['submit'])){ // Form has been submitted
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);

    $found_user= User::authenticate($username, $password);

    if($found_user){
        print_r($found_user);
        $session->login($found_user);
        $logger->log_action("Login", "UserID ". $session->user_id ."|". $username." logged in!");
        redirect_to("index.php");
    }else{
        // username/password combo was not found in the database
        $message = "Username/password combination incorrect.";
    }
}else{
    $username = "";
    $password = "";
}


include_layout_template('admin_header.php');
?>

<h2>Staff Menu</h2>
<?php if(isset($message)){echo output_message($message);}?>
<form action="login.php" method="POST">
    <table>
        <tr>
            <td>Username:</td>
            <td></td>
            <td>
                <input type="text" name="username" maxlength="30" value="<?php echo htmlentities($username);?>" />
            </td>
        </tr>
        <tr>
            <td>Password:</td>
            <td></td>
            <td>
                <input type="password" name="password" maxlength="30" value="<?php echo htmlentities($password);?>" />
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" name="submit" value="Login" />
            </td>
            <td>
                <a href="fg_pw.php">Passwort vergessen?</a>
            </td>
        </tr>
    </table>
</form>


<?php
include_layout_template('admin_footer.php');
?>