<?php
require_once("../../includes/initialize.php");


if(!$session->is_logged_in()){
    redirect_to("login.php");
}
$session->init();

if(!isset($_GET['id'])){
    $session->message("No User found, please try again!");
    redirect_to("user_control.php");
}else if($session->role->authorize_user()){
    $id = (int) $_GET['id'];
    $user = User::find_by_id($id);
    $user->init_role();
}else if($session->user_id === $_GET['id']){
    $user = $session->user;
}

if(isset($_POST['submit']) && $user instanceOf User){
    $n_pw = $_POST['pw'];
    $n_mail = $_POST['mail'];
    $create_vid = $_POST['create_vid'];
    $create_pho = $_POST['create_pho'];
    $create_user = $_POST['create_user'];
    $admin = $_POST['admin'];
    $pw_ch = false;
    $m_ch = false;

    if($n_pw !== $user->password && isset($_POST['pw_re']) && $n_pw === $_POST['pw_re']){
        $user->password = $n_pw;
        $message = "The password was changed!";
        $pw_ch= true;
    }
    elseif($n_pw !== $user->password && isset($_POST['pw_re'])) {
        $message = "The passwords were not equal!";
    }
    elseif($n_mail !== $user->mail){
        $m_ch = $user->set_mail($n_mail);
        $message .= ($m_ch) ? "The Mail was successfully changed." : "An Error occured.";
    }

    $message .= $user->set_role($create_pho, $create_vid, $create_user);


    if($admin !== $user->admin){
        $user->admin = $admin;
    }

    if($user->save()){
        $logger->log_action("Edit User", "UserID ". $session->user_id ." | ". $user->username." has been edited (" .($pw_ch ? "Password " : "" ) . ($m_ch? "Mail" : "").")!");
    }
}


include_layout_template('admin_header.php');
?>

<h2>User edit of <?php echo $user->full_name()." (".$user->username.")";?></h2>
<?php if(isset($message)){echo output_message($message);}?>

<form action="user_edit.php?id=<?php echo $id;?>" method="POST">
    <p style="border: 1px solid black; text-align: center; width: 400px; padding: 10px;">
        Username: <?php echo $user->username;?>
    </p>
    <p style="border: 1px solid black; text-align: center; width: 400px; padding: 10px;">
        Password: <input type="password" name="pw" value="<?php echo $user->password;?>"><br/>
        Password wiederholen: <input type="password" name="pw_re" /><br/><br/>
        Zum &auml;ndern des Passwortes ein neues Passwort eingeben und wiederholen!
    </p>
    <p style="border: 1px solid black; text-align: center; width: 400px; padding: 10px;">
        Email-Adresse: <input type="text" name="mail" value="<?php echo $user->mail; ?>">
    </p>
    <?php
    if($session->role->authorize_user() && $user->authorize_admin() != 2){
    ?>
        <p>
            Videos hochladen:
            <select name="create_vid" >
                <option value="1" <?php if($user->rRights->authorize_video()){echo "selected";}?> >Ja</option>
                <option value="0" <?php if(!$user->rRights->authorize_video()){echo "selected";}?> >Nein</option>
            </select>
        </p>
        <p>
            Bilder hochladen:
            <select name="create_pho" >
                <option value="1" <?php if($user->rRights->authorize_photo()){echo "selected";}?> >Ja</option>
                <option value="0" <?php if(!$user->rRights->authorize_photo()){echo "selected";}?> >Nein</option>
            </select>
        </p>
        <p>
            User editieren:
            <select name="create_user" >
                <option value="1" <?php if($user->rRights->authorize_user()){echo "selected";}?> >Ja</option>
                <option value="0" <?php if(!$user->rRights->authorize_user()){echo "selected";}?> >Nein</option>
            </select>
        </p>
        <?php if($user->authorize_admin()){ ?>
            <p>
                Admin:
                <select name="admin" >
                    <option value="1" <?php if($user->authorize_admin()){echo "selected";}?> >Ja</option>
                    <option value="0" <?php if(!$user->authorize_admin()){echo "selected";}?> >Nein</option>
                </select>
            </p>
        <?php } ?>
        </p>
    <?php } ?>

    <p>
        <input type="submit" name="submit" value="Update!" />
    </p>
</form>
<br>
<a href="user_control.php">Zur&uuml;ck</a>

<?php
include_layout_template('admin_footer.php');
?>