<?php
require_once("../../includes/initialize.php");

if(!$session->is_logged_in()){
    redirect_to("login.php");
}

if(empty($_GET['pho_id']) && empty($_GET['vid_id'])){
    $session->message("No ID was provided.");
    redirect_to('index.php');
}

if(isset($_GET['pho_id'])){

    $photo = Photograph::find_by_id($_GET['pho_id']);
    if(!$photo){
        $session->message("The photo could not be located.");
        redirect_to('index.php');
    }

    $comments = $photo->comments();
}else {
    $video = Video::find_by_id($_GET['vid_id']);
    if(!$video){
        $session->message("The video could not be located.");
        redirect_to('index.php');
    }

    $comments = $video->comments();
}
include_layout_template('admin_header.php');
?>

<h2>Comments on <?php echo isset($photo->filename) ? $photo->filename : $video->filename;?></h2>
<?php if(isset($message)){echo output_message($message);}?>
<p>
    <a href="list_<?php
        if(isset($_GET['pho_id'])){
            echo "photos";
        }else{
            echo "videos";
        }?>.php">
        &laquo; Back
    </a>
</p>

<br />

<div id="comments">
    <?php foreach ($comments as $comment):?>
        <div class="comment" style="margin-bottom: 2em;">
            <div class="author">
                <?php echo htmlentities($comment->author);?> wrote:
            </div>
            <div class="body">
                <?php echo strip_tags($comment->body, '<strong><em><p>')?>
            </div>
            <div class="meta-info" style="font-size: 0.8em;">
                <?php echo datetime_to_text($comment->created); ?>
            </div>
            <div class="actions" style="font-size: 0.8em;">
                <a href="delete_comment.php?id=<?php echo $comment->id;?>">
                    Delete Comment
                </a>
            </div>
        </div>
    <?php endforeach;
    if(empty($comments)){echo "No Comments!";}?>
</div>

<?php include_layout_template('admin_footer.php') ?>