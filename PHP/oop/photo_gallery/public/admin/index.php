<?php
require_once("../../includes/initialize.php");

if(!$session->is_logged_in()){
    echo "".$session->is_logged_in();
    redirect_to("login.php");
}
$session->init();

$user = User::find_by_id($session->user_id);
$user->init_role();

include_layout_template('admin_header.php');
?>

<h2>Menu</h2>
<?php if(isset($message)){echo output_message($message);}?>

<a href="logfile.php">Logfile</a><br />
<?php
if($session->role->authorize_photo()){
?>
<a href="list_photos.php">Photogallery</a><br />
<?php
}
if($session->role->authorize_video()){
?>
<a href="list_videos.php">Videogallery</a><br />
<?php
}
if($session->role->authorize_user()){
?>
<a href="user_control.php">User Control</a><br />
<?php
}
?>
<br>

<a href="user_edit.php?id=<?php echo $user->id;?>">Mein Profil</a>

<br>
<br>
<a href="logout.php">Log out</a><br />

<?php
include_layout_template('admin_footer.php');
?>