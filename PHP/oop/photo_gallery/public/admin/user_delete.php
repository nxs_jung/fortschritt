<?php
require_once("../../includes/initialize.php");

if(!$session->is_logged_in()){
    redirect_to("login.php");
}

if(!isset($_GET['id'])){
    $session->message("No User ID was provided.");
    redirect_to('user_control.php');
}

$id = (int) $_GET['id'];

$user = User::find_by_id($id);

if($user->delete()){
    $logger->log_action("Delete User", "UserID ". $session->user_id ." | ". $user->username." has been deleted!");
    $session->message("The User was succesfully deleted.");
    redirect_to('user_control.php');
}else{
    $sMessge = "The User could not be deleted.";
    $sTo = 'user_control.php';
}
$session->message($sMessge);
redirect_to($sTo);
