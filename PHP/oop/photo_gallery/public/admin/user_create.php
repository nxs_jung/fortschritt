<?php
require_once("../../includes/initialize.php");

if(!$session->is_logged_in()){
    redirect_to("login.php");
}

$session->init();

if(isset($_POST['submit'])){
    $user = new User();
    $user->rRights = new Role();
    $errors = array();

    $username = $_POST['username'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $mail = $_POST['mail'];
    $create_vid = $_POST['create_vid'];
    $create_pho = $_POST['create_pho'];
    $create_user = $_POST['create_user'];
    $admin = $_POST['admin'];

    $validator = new User_Validator($user);
    $errors = $validator->validate($_POST);

    if(count($errors) < 1 && $user->save()){
        $logger->log_action("Create User", "UserID ". $session->user_id ." | ". $user->username." has been created !");
        $session->message("The User was successfully created!");
        redirect_to("user_control.php");
    }else{
        foreach ($errors as $msg) {
            $message .= $msg."<br>";
        }
    }
}else{
    $username = "";
    $first_name = "";
    $last_name = "";
    $mail = "";
}


include_layout_template('admin_header.php');
?>
<h2>User creation</h2>
<?php if(isset($message)){echo output_message($message);}?>

<form action="user_create.php" method="POST">
    <p>
        Username: <input type="text" name="username" value="<?php echo $username;?>" />
    </p>
    <p>
        Password: <input type="password" name="password" value=""/>
    </p>
    <p>
        First name: <input type="text" name="first_name" value="<?php echo $first_name;?>"/>
    </p>
    <p>
        Last name: <input type="text" name="last_name" value="<?php echo $last_name;?>"/>
    </p>
    <p>
        Mail: <input type="text" name="mail" value="<?php echo $mail;?>"/>
    </p>
    <p>
        Soll der User Videos hochladen k&ouml;nnen?<br/>
        <select name="create_vid" >
            <option value="1">Ja</option>
            <option value="0">Nein</option>
        </select>
    </p>
    <p>
        Soll der User Bilder hochladen k&ouml;nnen?<br/>
        <select name="create_pho" >
            <option value="1">Ja</option>
            <option value="0">Nein</option>
        </select>
    </p>
    <p>
        Soll der User andere User bearbeiten k&ouml;nnen?<br/>
        <select name="create_user" >
            <option value="0">Nein</option>
            <option value="1">Ja</option>
        </select>
    </p>
    <p>
        Ist der User ein admin?<br/>
        <select name="admin" >
            <option value="0">Nein</option>
            <option value="1">Ja</option>
        </select>
    </p>
    <p>
        <input type="submit" name="submit" value="Erstellen" />
    </p>
</form>
<br>
<a href="user_control.php">Zur&uuml;ck</a>

<?php
include_layout_template('admin_footer.php');
?>