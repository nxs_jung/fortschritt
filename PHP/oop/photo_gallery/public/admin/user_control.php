<?php
require_once("../../includes/initialize.php");

if(!$session->is_logged_in()){
    redirect_to("login.php");
}

$session->init();
if(!$session->role->authorize_user()){
    redirect_to("index.php");
}

$user_set = User::find_all();


include_layout_template('admin_header.php');
?>

<h2>Users of the Photogallery</h2>
<?php if(isset($message)){echo output_message($message);}?>

<?php
    if(isset($user_set)){
?>
<ul>

<?php
    foreach ($user_set as $user) {
        echo "<li id=\"".$user->id."\">".$user->username." | ".$user->full_name()." \t <a href=\"user_edit.php?id=".$user->id."\">edit</a>". (($user->admin === "2")? "" :
        "  |  <a href=\"user_delete.php?id=".$user->id."\">delete</a>")
        ."</li>";
    }
?>

</ul>
<?php
}else{
    echo "No User available.";
}
?>
<br>
<a href="user_create.php">User erstellen!</a>
<br>
<br>
<a href="index.php">Zur&uuml;ck</a>

<?php
include_layout_template('admin_footer.php');
?>