<?php
require_once("../includes/initialize.php");

    if(!isset($_GET['h'])){
        redirect_to("index.php");
    }

    $hash = $_GET['h'];
    $hash_link = strstr($hash, "|", true);
    $id = (int) substr(strstr($hash, "|"), 1);


    if(!User::check_hash($id, $hash_link)){
        redirect_to("index.php");
    }


    if(isset($_POST['submit']) && User::check_hash($id, $hash_link)){

        $user = User::find_by_id($id);
        $new_password = trim($_POST['new_password']);
        $new_password_re = trim($_POST['new_password_re']);

        if (($new_password === $new_password_re)) {
            $user->password = $new_password;


            if ($user->save()) {
                //Sucess!
                $message = "Yout password was successfully reseted.";
            }else{
                //Display ERROR
                $message = "<p> Password reset failed.</p>";
                $message .= "<p> ".mysql_error()." </p>";
            }
        }else{
            $message = "The passwords were not equal!";
        }

    }elseif( isset($_POST['submit']) ){
        $message = "The link is not correct! \n Please try it again.";
    }else{
        $new_password ="";
    }

    include_layout_template('header.php');

    ?>
<h2>Passwort vergessen? Kein Problem - geben sie einfach unten ein neues ein!</h2>
<?php if(isset($message)){echo output_message($message);}?>
<form action="reset_pw.php?h=<?php echo $hash;?>" method="POST">
    <p>
        Password: <input type="password" name="new_password" value="<?php echo $new_password;?>"/>
    </p>
    <p>
        Passwort wiederholen: <input type="password" name="new_password_re" />
    </p>
    <p>
        <input type="submit" name="submit" value="Passwort &auml;ndern!">
    </p>
</form>

<?php
include_layout_template('footer.php');