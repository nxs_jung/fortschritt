<?php
require_once("../includes/initialize.php");

// 1. the current page number ($current_page)
$pho_page = !empty($_GET['pho_page']) ? (int)$_GET['pho_page'] : 1;
$vid_page = !empty($_GET['vid_page']) ? (int)$_GET['vid_page'] : 1;

// 2. records per page ($per_page)
$per_page = 3;

// 3. total record count ($total_count)
$total_count_pho = Photograph::count_all();
$total_count_vid = Video::count_all();

// Find all photos
// use pagination instead
// $photos = Photograph::find_all();

$pho_pagination = new Pagination($pho_page, $per_page, $total_count_pho);
$vid_pagination = new Pagination($vid_page, $per_page, $total_count_vid);

// Instead if findíng all records, just find the records for this page
$sql = "SELECT * FROM photographs ";
$sql .= " LIMIT ".$per_page;
$sql .= " OFFSET ".$pho_pagination->offset();
$photos = Photograph::find_by_sql($sql);

// Need to add ?page=$page to all links we want to
// maintain the current page (or store $page in $session)

// Instead if findíng all records, just find the records for this page
$sql_v = "SELECT * FROM videos ";
$sql_v .= " LIMIT ".$per_page;
$sql_v .= " OFFSET ".$vid_pagination->offset();
$videos = Video::find_by_sql($sql_v);



include_layout_template('header.php');

foreach ($photos as $photo):?>

<?php if(isset($message)){echo output_message($message);}?>

<div style="float: left; margin-left: 20px;">
    <a href="photo.php?id=<?php echo $photo->id; ?>">
        <img src="<?php echo $photo->image_path(); ?>" alt="<?php echo $photo->filename;?>" width="200">
    </a>
    <p><?php echo $photo->caption; ?></p>
</div>

<?php endforeach;?>

<div id="pagination" style="clear: both;">
    <?php
        if($pho_pagination->total_pages() > 1){
            if($pho_pagination->has_previous_page()){
                echo "<a href=\"index.php?pho_page=";
                echo $pho_pagination->previous_page();
                echo "\">&laquo; Previous</a>   ";
            }

            for($i = 1; $i <= $pho_pagination->total_pages(); $i++){
                if($i == $page){
                    echo "<span class=\"selected\">".$i."</span> ";
                }else{
                    echo " <a href=\"index.php?pho_page=".$i."\">".$i."</a> ";
                }
            }

            if($pho_pagination->has_next_page()){
                echo "   <a href=\"index.php?pho_page=";
                echo $pho_pagination->next_page();
                echo "\">Next &raquo;</a>";
            }
        }
    ?>
</div>

<?php foreach ($videos as $video):?>

<?php if(isset($message)){echo output_message($message);}?>

<div style="float: left; margin-left: 20px; margin-top: 100px;">
    <a href="video.php?id=<?php echo $video->id; ?>">
        <video src="<?php echo $video->image_path(); ?>" alt="<?php echo $video->filename;?>" width="200"></video>
    </a>
    <p><?php echo $video->caption; ?></p>
</div>

<?php endforeach;?>

<div id="pagination" style="clear: both;">
    <?php
        if($vid_pagination->total_pages() > 1){
            if($vid_pagination->has_previous_page()){
                echo "<a href=\"index.php?vid_page=";
                echo $vid_pagination->previous_page();
                echo "\">&laquo; Previous</a>   ";
            }

            for($i = 1; $i <= $vid_pagination->total_pages(); $i++){
                if($i == $page){
                    echo "<span class=\"selected\">".$i."</span> ";
                }else{
                    echo " <a href=\"index.php?vid_page=".$i."\">".$i."</a> ";
                }
            }

            if($vid_pagination->has_next_page()){
                echo "   <a href=\"index.php?vid_page=";
                echo $vid_pagination->next_page();
                echo "\">Next &raquo;</a>";
            }
        }
    ?>
</div>

<?php
include_layout_template('footer.php');