<?php

class Beverage {
    public $name;

    function __construct(){
        echo "New ".get_class()." object created!" . PHP_EOL;
    }

    function __clone(){
        echo "The class ".get_class()." was cloned!" . PHP_EOL;
    }
}

$a = new Beverage();
$a -> name = "coffe";
$b = $a;
$b -> name = "tea";

echo $a -> name . PHP_EOL;

$c = clone $a;
$c -> name = "orange juice";

echo $a -> name . PHP_EOL;
echo $c -> name . PHP_EOL;