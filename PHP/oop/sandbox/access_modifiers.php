<?php

class Example {
    public $a = 1;
    protected $b = 2;
    private $c = 3;

    function show_abc(){
        echo $this -> a;
        echo $this -> b;
        echo $this -> c;
    }

    public function hello_everyone(){
        return "Hello everyone!".PHP_EOL;
    }

    protected function hello_family(){
        return "Hello family!".PHP_EOL;
    }

    private function hello_me(){
        return "Hello me!".PHP_EOL;
    }

    function hello(){
        $output = $this -> hello_everyone();
        $output .= $this -> hello_family();
        $output .= $this -> hello_me();
        return $output;
    }
}

$ex = new Example();
$ex -> show_abc();
echo PHP_EOL;
echo $ex -> a  . PHP_EOL;
#echo $ex -> b  . PHP_EOL;      //Protected
#echo $ex -> c . PHP_EOL;       //Private

echo $ex -> hello();
echo $ex -> hello_everyone();
#echo $ex -> hello_family();    //Protected
#echo $ex -> hello_me();        //Private