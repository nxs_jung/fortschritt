<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Reference as Function Arguments</title>
</head>
<body>
<?php

function ref_test(&$var){
    $var = $var * 2;
}

$a = 10;
ref_test($a);

echo $a . PHP_EOL;

?>
</body>
</html>