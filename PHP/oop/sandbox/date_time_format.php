<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dates and Time: Formatting</title>
</head>
<body>
<?php
    function strip_zeros_from_date($marked_string='')
    {
        // remove the marked zeros
        $no_zeros = str_replace('*0', '', $marked_string);
        //remove any remaining marks
        $cleaned_string = str_replace('*', '', $no_zeros);
        return $cleaned_string;
    }

    $timestamp = mktime(0,0,0,2,9,2014);

    echo strftime("The day is %d/%m/%y ", $timestamp);
    echo PHP_EOL;
    echo strip_zeros_from_date(strftime("The day is *%d/*%m/%y ", $timestamp));
    echo PHP_EOL;
    echo PHP_EOL;
    echo PHP_EOL;

    $dt = time();
    $mysql_datetime = strftime("%Y-%m-%d %H:%M:%S", $dt);
    echo $mysql_datetime;
    echo PHP_EOL;

    ?>
</body>
</html>