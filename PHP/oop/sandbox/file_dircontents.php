<?php

// Like fopen/fread/fclose:
// opendir()
// readdir()
// closedir()

$dir = ".";

if(is_dir($dir)){
    if($dir_handle = opendir($dir)){
        while ($filename = readdir($dir_handle)) {
            echo "filename: ".$filename. PHP_EOL;
        }
        // use rewinddir($dir_handle) to start over
        closedir($dir_handle);
    }
}

echo "_______________________________________________".PHP_EOL;
echo "-----------------------------------------------".PHP_EOL;
echo "_______________________________________________".PHP_EOL;


// scandir(): reads all filenames into an array
if(is_dir($dir)){
    $dir_array = scandir($dir);
    foreach ($dir_array as $file) {
        if(stripos($file, '.')>0){
            echo "filename: ".$file. PHP_EOL;
        }
    }
}