<?php

class Person
{
    function say_hello(){
        echo "Hello from inside a class".PHP_EOL;
    }
}

$methods = get_class_methods('Person');
foreach ($methods as $method) {
    echo $method . PHP_EOL;
}

if (method_exists('Person', 'say_hello')) {
    echo "Method does exists".PHP_EOL;
}else{
    echo "Method does not exists.".PHP_EOL;
}
