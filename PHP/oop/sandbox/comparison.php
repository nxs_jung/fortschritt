<?php

class Box{
    public $name = "box";
}

$box = new Box();
$box_reference = $box;
$box_clone = clone $box;

$box_changed = clone $box;
$box_changed -> name = "changed box";

$another_box = new Box();

// == is casual and just checks to see if the attributes are the same
echo $box == $box_reference ? 'true' : 'false';
echo PHP_EOL;
echo $box == $box_clone ? 'true' : 'false';
echo PHP_EOL;
echo $box == $box_changed ? 'true' : 'false';
echo PHP_EOL;
echo $box == $another_box ? 'true' : 'false';
echo PHP_EOL;
echo PHP_EOL;

// === is strict and checks if they reference to the same object
echo $box === $box_reference ? 'true' : 'false';
echo PHP_EOL;
echo $box === $box_clone ? 'true' : 'false';
echo PHP_EOL;
echo $box === $box_changed ? 'true' : 'false';
echo PHP_EOL;
echo $box === $another_box ? 'true' : 'false';
echo PHP_EOL;
echo PHP_EOL;