<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Reference as Function Return Values</title>
</head>
<body>
<?php

function &ref_return(){
    global $a;
    $a = $a*2;
    return $a;
}

$a = 10;
$b =& ref_return();

echo "a: ".$a."/ b: ".$b.PHP_EOL; echo "<br>";

echo "-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-".PHP_EOL;

$b = 30;

echo "a: ".$a."/ b: ".$b.PHP_EOL; echo "<br>";

echo "-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-".PHP_EOL;

function &increment(){
    static $var = 0;
    $var++;
    return $var;
}

$a =& increment();
increment();
$a++;
increment();
echo "a: ".$a.PHP_EOL;

echo "-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-".PHP_EOL;

?>
</body>
</html>