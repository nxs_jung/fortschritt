<?php

class SetterGetterExample {
    private $a = 1;

    public function get_a(){
        return $this -> a;
    }

    public function set_a($value){
        $this -> a = $value;
    }
}

$ex = new SetterGetterExample();

echo $ex -> get_a() . PHP_EOL;
$ex -> set_a(15);
echo $ex -> get_a() . PHP_EOL;