<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Server Variables</title>
</head>
<body>
<?php
echo "Server details: ".PHP_EOL; echo "<br>";
echo "SERVER_NAME: ". $_SERVER['SERVER_NAME'].PHP_EOL; echo "<br>";
echo "SERVER_ADDR: ". $_SERVER['SERVER_ADDR'].PHP_EOL; echo "<br>";
echo "SERVER_PROT: ". $_SERVER['SERVER_PORT'].PHP_EOL; echo "<br>";
echo "DOCUMENT_ROOT: ". $_SERVER['DOCUMENT_ROOT'].PHP_EOL; echo "<br>";
echo "<br>";
echo "Page details: ".PHP_EOL; echo "<br>";
echo "PHP_SELF: ".$_SERVER['PHP_SELF'].PHP_EOL; echo "<br>";
echo "SCRIPT_FILENAME: ".$_SERVER['SCRIPT_FILENAME'].PHP_EOL; echo "<br>";
echo "<br>";
echo "Request details: ".PHP_EOL; echo "<br>";
echo "REMOTE_ADDR: ". $_SERVER['REMOTE_ADDR'].PHP_EOL; echo "<br>";
echo "REMOTE_PORT: ". $_SERVER['REMOTE_PORT'].PHP_EOL; echo "<br>";
echo "REQUEST_URI: ". $_SERVER['REQUEST_URI'].PHP_EOL; echo "<br>";
echo "QUERY_STRING: ". $_SERVER['QUERY_STRING'].PHP_EOL; echo "<br>";
echo "REQUEST_METHOD: ". $_SERVER['REQUEST_METHOD'].PHP_EOL; echo "<br>";
echo "REQUEST_TIME: ". $_SERVER['REQUEST_TIME'].PHP_EOL; echo "<br>";
echo "HTTP_REFERER: ". $_SERVER['HTTP_REFERER'].PHP_EOL; echo "<br>";
echo "HTTP_USER_AGENT: ". $_SERVER['HTTP_USER_AGENT'].PHP_EOL; echo "<br>";
    ?>
</body>
</html>