<?php

class Person
{
    function say_hello(){
        echo "Hello from inside the class ".get_class($this).PHP_EOL;
    }

    function hello(){
        $this -> say_hello();
    }
}

$person = new Person();
$person2 = new Person();

$person -> hello();
