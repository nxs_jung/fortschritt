<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dates and Time: Unix</title>
</head>
<body>
<?php
    echo time();
    echo PHP_EOL;
    echo mktime(2, 30, 45, 10, 1, 2009);
    echo PHP_EOL;
    // checkdate
    echo checkdate(12, 31, 2000) ? 'true' : 'false';
    echo PHP_EOL;
    echo checkdate(2, 31, 2000) ? 'true' : 'false';
    echo PHP_EOL;

    $unix_timestamp = strtotime("now");
    echo $unix_timestamp . PHP_EOL;
?>
</body>
</html>