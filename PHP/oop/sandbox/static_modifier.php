<?php
// with static methods you can't use $this!
class Student {
    static $total_students = 0;

    static public function add_student(){
        self::$total_students++;
    }

    static function welcome_students($var = "Hello"){
        echo $var. " Student.";
    }
}

// $student = new Student();                            //instance call
// echo $student -> total_student;

echo Student::$total_students . PHP_EOL;                // static call
echo Student::welcome_students() . PHP_EOL;
echo Student::welcome_students("Greetings") . PHP_EOL;
Student::$total_students= 1;
echo Student::$total_students . PHP_EOL;


// static variables are shared throughout the inheritance tree

class One{
    static $foo;
}

class Two extends One {}

class Three extends One {}

One::$foo = 1;
Two::$foo = 2;
Three::$foo = 3;

echo One::$foo . PHP_EOL;
echo Two::$foo . PHP_EOL;
echo Three::$foo . PHP_EOL;