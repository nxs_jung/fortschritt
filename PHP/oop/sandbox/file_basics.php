<?php

echo __FILE__ . PHP_EOL;
echo __LINE__ . PHP_EOL;            // be careful once you include files
echo dirname(__FILE__) . PHP_EOL;
echo __DIR__ . PHP_EOL;             // since 5.3
echo PHP_EOL;

echo file_exists(__FILE__) ? 'yes'.PHP_EOL : 'no'.PHP_EOL ;
echo file_exists(__DIR__ . '/basic.html') ? 'yes'. PHP_EOL : 'no'.PHP_EOL ;
echo file_exists(__DIR__) ? 'yes'. PHP_EOL : 'no'.PHP_EOL ;
echo PHP_EOL;

echo is_file(__DIR__ . '/basic.html') ? 'yes'. PHP_EOL : 'no'.PHP_EOL ;
echo is_file(__DIR__) ? 'yes'. PHP_EOL : 'no'.PHP_EOL ;
echo PHP_EOL;

echo is_dir(__DIR__ . '/basic.html') ? 'yes'. PHP_EOL : 'no'.PHP_EOL ;
echo is_dir(__DIR__) ? 'yes'. PHP_EOL : 'no'.PHP_EOL ;
echo is_dir('..') ? 'yes'. PHP_EOL : 'no'.PHP_EOL ;
echo PHP_EOL;