<?php

class Person
{
    function say_hello(){
        echo "Hello from inside a class".PHP_EOL;
    }
}

$person = new Person();
$person2 = new Person();

echo get_class($person) . PHP_EOL;

if(is_a($person, 'Person')){
    echo "Yup, its a Person".PHP_EOL;
}else{
    echo "Not a Person".PHP_EOL;
}

$person -> say_hello();