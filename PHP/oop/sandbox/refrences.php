<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Reference Assignment</title>
</head>
<body>
<?php

$a = 1;
$b = $a;
$b = 2;

echo "a: ".$a."/ b: ".$b.PHP_EOL; echo "<br>";

echo "-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-".PHP_EOL;

$a = 1;
$b =& $a;
$b = 2;

echo "a: ".$a."/ b: ".$b.PHP_EOL; echo "<br>";

echo "-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-".PHP_EOL;

unset($b);

echo "a: ".$a."/ b: ".$b.PHP_EOL; echo "<br>";

?>
</body>
</html>