<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Variable Scope</title>
</head>
<body>
<?php

$var = 2;

function test1(){
    $var = 1;
    echo $var;
    echo PHP_EOL;
}

test1();
echo $var;
echo PHP_EOL;

echo "-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-".PHP_EOL;

$var = 2;

function test2(){
    global $var;
    $var = 1;
    echo $var;
    echo PHP_EOL;
}

test2();
echo $var;
echo PHP_EOL;

echo "-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-".PHP_EOL;

$var = 2;

function test3(){
    static $var = 1;
    echo $var;
    echo PHP_EOL;
    $var++;
}

test3();
test3();
test3();
echo $var;
echo PHP_EOL;


?>
</body>
</html>