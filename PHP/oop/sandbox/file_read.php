<?php

$file= 'filetest.txt';

if($handle = fopen($file, 'r')){    // read
    $content = fread($handle, 6);              // each character is 1 byte
    fclose($handle);
}else{
    echo "Could not open file for writing!";
}

echo $content . "" . PHP_EOL;
echo nl2br($content) . "" . PHP_EOL;
echo PHP_EOL;


// use filesize() to read the whole file

$file= 'filetest.txt';

if($handle = fopen($file, 'r')){    // read
    $content = fread($handle, filesize($file));              // each character is 1 byte
    fclose($handle);
}else{
    echo "Could not open file for writing!";
}

echo $content . "" . PHP_EOL;
echo PHP_EOL;


// file_get_contents(): shortcut for fopen/fread/fclose
// companion to shortcut file_put_contents()

$content = file_get_contents($file);
echo $content . "file_get_contents" . PHP_EOL;
echo PHP_EOL;


// incremental reading

$file= 'filetest.txt';

if($handle = fopen($file, 'r')){    // read
    while(!feof($handle)){
        $content .= fgets($handle);
    }
    fclose($handle);
}else{
    echo "Could not open file for writing!";
}

echo $content . "a" . PHP_EOL;
echo PHP_EOL;