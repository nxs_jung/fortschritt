<?php

require_once('PHPMailer/class.phpmailer.php');
require_once('PHPMailer/class.smtp.php');
require_once('PHPMailer/language/phpmailer.lang-de.php');


$to_name = "TEST";
$to = "momo.jung@live.de";

$subject = "Mail test at " . strftime("%T", time());

$message = "This is a test!";

$message = wordwrap($message, 70);

$from_name = "Moritz Jung";
$from = "jung@nexus-netsoft.com";

// PHP mail version (default)
$mail = new PHPMailer();

// $mail->IsSMTP();
// $mail->Host     = "smtp.gmail.com";
// $mail->Port     = 465;
// $mail->SMTPAuth = true;
// $mail->Username = "jung@nexus-netsoft.com";
// $mail->Password = "";

$mail->FromName = $from_name;
$mail->From     = $from;
$mail->addAddress($to, $to_name);
$mail->Subject  = $subject;
$mail->Body     = $message;


$result = $mail->send();

echo ($result) ? "Sent" : "Not sent";