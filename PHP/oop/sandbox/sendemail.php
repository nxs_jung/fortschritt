<?php

$to = "jung@nexus-netsoft.com";

// Windows may not handle this format well
// $to = "Moritz Jung <jung@nexus-netsoft.com>";
//
// multiple recipients
// $to = "junkmail@novafabrica.com, nobody@novafabrica.com";
// $to = "Moritz Jung <jung@nexus-netsoft.com>, junkmail@novafabrica.com, nobody@novafabrica.com";

$subject = "Mail test at " . strftime("%T", time());

$message = "This is a test!";
// Optional: Wrap lines for old email programs
// wrap at 70/72/75/78
$message = wordwrap($message, 70);

$from = "noreply@bliblablu.com";
$reply_to = "momo.jung@live.de";
$headers = "From: ".$from."\n";
$headers .= "Reply-to: ".$reply_to."\n";
// $headers .= "Cc: ".$reply_to."\n";
// $headers .= "Bcc: ".$reply_to."\n";
$headers .= "X-Mailer: PHP/".phpversion()."\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-Type: text/plain; charset=iso-8859-1\n";

$result = mail($to, $subject, $message, $headers);

echo ($result) ? "Sent" : "Not sent";