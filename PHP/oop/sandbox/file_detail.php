<?php

$file = 'filetest.txt';

echo filesize($file). PHP_EOL;      // in bytes

// filemtime: last modified (changed content)               --> cached value
// filectime: last changed (changed content or metadata)    --> cached value
// fileatime: last accessed (andy read/change)              --> cached value

echo strftime('%m/%d/%Y %H:%M', filemtime($file)) . PHP_EOL;
echo strftime('%m/%d/%Y %H:%M', filectime($file)) . PHP_EOL;
echo strftime('%m/%d/%Y %H:%M', fileatime($file)) . PHP_EOL;

touch($file);

echo strftime('%m/%d/%Y %H:%M', filemtime($file)) . PHP_EOL;
echo strftime('%m/%d/%Y %H:%M', filectime($file)) . PHP_EOL;
echo strftime('%m/%d/%Y %H:%M', fileatime($file)) . PHP_EOL;

$path_parts = pathinfo(__FILE__);
echo $path_parts['dirname'] . PHP_EOL;
echo $path_parts['basename'] . PHP_EOL;
echo $path_parts['filename'] . PHP_EOL;
echo $path_parts['extension'] . PHP_EOL;
