<?php

class Person
{
    var $first_name;
    var $last_name;

    var $arm_count = 2;
    var $leg_count = 2;

    function full_name(){
        return $this -> first_name . " " . $this -> last_name . PHP_EOL;
    }

    function say_hello(){
        echo "Hello from inside the class ".get_class($this).PHP_EOL;
    }

    function hello(){
        $this -> say_hello();
    }
}

$person = new Person();

echo $person -> arm_count;
echo PHP_EOL;

$person -> arm_count = 3;
$person -> first_name = 'Lucy';
$person -> last_name = 'Ricardo';

$new_person = new Person();
$new_person -> first_name = 'Ethel';
$new_person -> last_name = 'Mertz';

echo $person -> full_name();
echo PHP_EOL;
echo $new_person -> full_name();
echo PHP_EOL;

$vars = get_class_vars('Person');
foreach ($vars as $var => $value) {
    echo $var.": ".$value.PHP_EOL;
}
