<?php

echo fileowner('file_permissions.php') . PHP_EOL;

// if we have Posix installed

$ownder_id = fileowner('file_permissions.php');
//$owner_array = posix_getpwuid($owner_id);
//echo $owner_array['name'] . PHP_EOL;

chown('file_permissions.php', 'moritz.jung');
// chown only works if PHP is superuser
// making webserver/PHP a superuser is a big security issue

// if we have Posix installed

$ownder_id = fileowner('file_permissions.php');
//$owner_array = posix_getpwuid($owner_id);
//echo $owner_array['name'] . PHP_EOL;

echo substr(decoct(fileperms('file_permissions.php')),2).PHP_EOL;
chmod('file_permissions.php', 0777);
echo substr(decoct(fileperms('file_permissions.php')),2).PHP_EOL;

echo is_readable('file_permissions.php') ? 'yes' .PHP_EOL: 'no' .PHP_EOL;
echo is_writable('file_permissions.php') ? 'yes' .PHP_EOL: 'no' .PHP_EOL;