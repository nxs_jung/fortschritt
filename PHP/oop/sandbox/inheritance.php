<?php

class Car {
    var $wheels = 4;
    var $doors = 4;

    function wheeldoors(){
        return $this-> wheels + $this-> doors;
    }
}

class CompactCar extends Car {
    var $doors = 2;

    function wheeldoors(){
        return $this-> wheels + $this-> doors +100;
    }
}

$car1 = new Car();
$car2 = new CompactCar();

echo "car1". PHP_EOL;
echo $car1 -> wheels . PHP_EOL;
echo $car1 -> doors . PHP_EOL;
echo $car1 -> wheeldoors() . PHP_EOL;
echo PHP_EOL;

echo "car2". PHP_EOL;
echo $car2 -> wheels . PHP_EOL;
echo $car2 -> doors . PHP_EOL;
echo $car2 -> wheeldoors() . PHP_EOL;
echo PHP_EOL;

echo "Car parent: ". get_parent_class('Car') . PHP_EOL;
echo "CompactCar parent: ". get_parent_class('CompactCar') . PHP_EOL;
echo PHP_EOL;
echo is_subclass_of('Car', 'Car') ? 'true' : 'false';
echo PHP_EOL;
echo is_subclass_of('CompactCar', 'Car') ? 'true' : 'false';
echo PHP_EOL;
echo is_subclass_of('Car', 'CompactCar') ? 'true' : 'false';
echo PHP_EOL;
