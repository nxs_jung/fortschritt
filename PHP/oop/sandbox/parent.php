<?php

class A{
    static $a = 1;

    static function modified_a(){
        return self::$a + 10;
    }

    public function hello(){
        echo "Hello".PHP_EOL;
    }
}

class B extends A{
    static function attr_test(){
        echo parent::$a;
    }

    static function method_test(){
        echo parent::modified_a();
    }

    public function instance_test(){
        echo parent::hello();
    }

    public function hello(){
        echo "*********".PHP_EOL;
        parent::hello();
        echo "*********".PHP_EOL;
    }
}

echo B::$a . PHP_EOL;
echo B::modified_a() . PHP_EOL;
echo PHP_EOL;

echo B::attr_test() . PHP_EOL;
echo B::method_test() . PHP_EOL;

$object = new B();
$object -> instance_test();
$object -> hello();