<?php

class Table{
    public $legs;
    static public $total_tables=0;

    public function __construct($leg_count=4){
        $this -> legs = $leg_count;
        Table::$total_tables++;
    }

    function __destruct(){
        Table::$total_tables--;
    }
}

$table = new Table();
echo $table -> legs . PHP_EOL;

echo Table::$total_tables . PHP_EOL;
$t1 = new Table();
echo Table::$total_tables . PHP_EOL;
$t2 = new Table();
echo Table::$total_tables . PHP_EOL;